# **AnimalSaver**


## **Használati esetek**

 ### Nem regisztrált

> - regisztráció bejelentőnek/állatmentőnek
> - bejelentett állatok megtekintése
> - állatkórházak megtekintése
> - ismertető olvasása

 ### Bejelentő 
> - belépés
> - kilépés
> - adatmódosítás
> - fiók törlése
> - állat bejelentés
> - email tájékoztató
> - állatkózházak megtekintése
> - közeli állatvédő keresés
> - bejelentett állatok megtekintése (+száma)

 ### Állatmentő
> - belépés
> - kilépés
> - adatmódosítás
> - fiók törlése
> - állat bejelentés
> - email tájékoztató
> - állatkózházak megtekintése
> - közeli állatvédő keresés
> - bejelentett állatok megtekintése (+száma)
> - eddig gondozott állatok megtekintése 
> - állat gondozás (bejelentő info megtekintése)
> - állat elengedés/temetés
> - túlélési arány

