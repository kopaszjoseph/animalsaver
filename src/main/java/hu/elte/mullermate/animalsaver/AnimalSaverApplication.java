package hu.elte.mullermate.animalsaver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class AnimalSaverApplication {


	public static void main(String[] args) {
		SpringApplication.run(AnimalSaverApplication.class, args);
	}


}
