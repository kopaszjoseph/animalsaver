package hu.elte.mullermate.animalsaver.config;

import hu.elte.mullermate.animalsaver.repository.AnimalHospitalRepository;
import hu.elte.mullermate.animalsaver.repository.AnimalRepository;
import hu.elte.mullermate.animalsaver.repository.UserRepository;
import hu.elte.mullermate.animalsaver.security.MyUserDetailsService;
import hu.elte.mullermate.animalsaver.service.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;


/**
 * @author mullerm
 */
@Configuration
public class AnimalSaverApplicationConfiguration {

    @Bean
    public AnimalService animalService(AnimalRepository animalRepository, UserRepository userRepository) {
        return new AnimalServiceImpl(animalRepository, userRepository);
    }

    @Bean
    public UserService userService(UserRepository userRepository, AnimalRepository animalRepository, EmailService emailService, PasswordEncoder passwordEncoder) {
        return new UserServiceImpl(userRepository, animalRepository, emailService, passwordEncoder);
    }

    @Bean
    public AnimalHospitalService animalHospitalService(AnimalHospitalRepository animalHospitalRepository) {
        return new AnimalHospitalServiceImpl(animalHospitalRepository);
    }

    @Bean
    public EmailService emailService(JavaMailSender javaMailSender){
        return new EmailServiceImpl(javaMailSender);
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return PasswordEncoderFactories.createDelegatingPasswordEncoder();
    }

    @Bean
    public UserDetailsService userDetailsService(UserRepository userRepository) {
        return new MyUserDetailsService(userRepository);
    }


}




