package hu.elte.mullermate.animalsaver.domain;

import hu.elte.mullermate.animalsaver.enums.AnimalCategory;
import hu.elte.mullermate.animalsaver.enums.AnimalState;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Objects;


/**
 * Animal class that contains the attributes of an animal which will be stored in a database
 *
 * @author mullerm
 */

@Entity
@Data
public class Animal {

    @Id
    @GeneratedValue(generator = "ID_SEQ")
    @SequenceGenerator(name = "ID_SEQ")
    private Long id;

    @ManyToOne
    private User finder;

    private String finderName;

    private String finderPhone;

    private String zipCode;

    private String province;

    private String city;

    private String street;

    private String houseNumber;

    @Enumerated(EnumType.STRING)
    private AnimalCategory category;

    private String picturePath;

    private boolean damaged;

    private String comment;

    @Enumerated(EnumType.STRING)
    private AnimalState state;

    private LocalDate createdTime;


    public Animal() {
        setFinderName("unknown");
        this.state = AnimalState.NEW;
        this.category = AnimalCategory.UNKNOWN;
    }



    @Override
    public String toString() {
        return "Animal{" +
                "id=" + id +
                ", finderName='" + finderName + '\'' +
                ", zipCode='" + zipCode + '\'' +
                ", city='" + city + '\'' +
                ", street='" + street + '\'' +
                ", houseNumber='" + houseNumber + '\'' +
                ", category=" + category +
                ", picturePath='" + picturePath + '\'' +
                ", damaged=" + damaged +
                ", comment='" + comment + '\'' +
                ", state=" + state +
                ", createdTime=" + createdTime +
                '}';
    }

    public String foundAddress(){
        return province + " " + zipCode + " " + city + ", " + street + " " + houseNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Animal)) return false;
        Animal animal = (Animal) o;
        return isDamaged() == animal.isDamaged() &&
                getId().equals(animal.getId()) &&
                Objects.equals(getFinder(), animal.getFinder()) &&
                Objects.equals(getFinderName(), animal.getFinderName()) &&
                getCategory() == animal.getCategory() &&
                Objects.equals(getPicturePath(), animal.getPicturePath()) &&
                Objects.equals(getComment(), animal.getComment()) &&
                getState() == animal.getState() &&
                getCreatedTime().equals(animal.getCreatedTime());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getFinder(), getFinderName(), getCategory(), getPicturePath(), isDamaged(), getComment(), getState(), getCreatedTime());
    }
}

