package hu.elte.mullermate.animalsaver.domain;


import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;


/**
 * AnimalHospital class that contains the attributes of a hospital which will be stored in a database
 *
 * @author mullerm
 */
@Entity
@Data
public class AnimalHospital {

    @Id
    @GeneratedValue(generator = "ID_SEQ")
    @SequenceGenerator(name = "ID_SEQ")
    private Long id;
    private String name;
    private String province;
    private String city;
    private String address;
    private String website;
    private String email;
    private String phone;

    @Override
    public String toString() {
        return "AnimalHospital{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", province=" + province +
                ", city='" + city + '\'' +
                ", address='" + address + '\'' +
                ", website='" + website + '\'' +
                ", email='" + email + '\'' +
                ", phone='" + phone + '\'' +
                '}';
    }
}

