package hu.elte.mullermate.animalsaver.domain;

import hu.elte.mullermate.animalsaver.enums.UserType;
import lombok.Data;

import javax.persistence.*;
import java.util.List;


/**
 * User class that contains the attributes of a user which will be stored in a database
 * @author mullerm
 */
@Entity
@Data
public class User {

    @Id
    @GeneratedValue(generator = "ID_SEQ")
    @SequenceGenerator(name = "ID_SEQ")
    private Long id;

    private String name;

    private String email;

    private String phone;

    private String province;

    private String address;

    @Enumerated(EnumType.STRING)
    private UserType type;

    private String password;

    private double reportedCount;

    private double caredCount;

    private double deadCount;

    private double releasedCount;

    @OneToMany(mappedBy = "finder", fetch = FetchType.EAGER)
    private List<Animal> animals;

    private String authority;


    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", phone='" + phone + '\'' +
                ", province='" + province + '\'' +
                ", address='" + address + '\'' +
                ", type=" + type +
                ", role=" + authority +
                ", password='" + password + '\'' +
                ", reportedCount=" + reportedCount +
                ", caredCount=" + caredCount +
                ", deadCount=" + deadCount +
                ", releasedCount=" + releasedCount +
                '}';
    }

    public String toContact(){
        return "Név: " + name + "\n" +
                "Email: " + email + "\n" +
                "Telefonszám: " + phone + "\n" +
                "Lakcím: " + address + "\n";
    }
}
