package hu.elte.mullermate.animalsaver.enums;

import java.util.Arrays;
import java.util.List;

import static java.util.stream.Collectors.toList;

/**
 * @author mullerm
 */
public enum AnimalCategory {
    ARTHROPOD("Ízeltlábú"),
    FISH("Hal"),
    AMPHIBIAN("Kétéltű"),
    REPTILE("Hüllő"),
    BIRD("Madár"),
    MAMMAL("Emlős"),
    UNKNOWN("Ismeretlen");

    private final String label;

    AnimalCategory(String label) {
        this.label = label;
    }

    public static List<String> labelValues() {
        return Arrays.stream(values())
                .map(r -> r.label)
                .collect(toList());
    }

    public static String getLabel(AnimalCategory animalCategory) {
        return animalCategory.label;
    }

    public static AnimalCategory getEnumValue(String label) {
        if (label == null) return null;
        for (AnimalCategory category : AnimalCategory.values()) {
            if (getLabel(category).equals(label)) {
                return category;
            }
        }
        throw new IllegalArgumentException("No saved category with this label: " + label);
    }
}

