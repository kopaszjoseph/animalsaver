package hu.elte.mullermate.animalsaver.enums;


import java.util.Arrays;
import java.util.List;

import static java.util.stream.Collectors.toList;

/**
 * @author mullerm
 */
public enum AnimalState {
    NEW(""),
    CARE("Gondozás alatt"),
    DEAD("Elpusztult"),
    FOUND("Új"),
    RELEASED("Elengedve");

    private final String label;

    AnimalState(String label) {
        this.label = label;
    }

    public static List<String> labelValues() {
        return Arrays.stream(values())
                .map(r -> r.label)
                .collect(toList());
    }

    public static String getLabel(AnimalState animalState) {
        return animalState.label;
    }

    public static AnimalState getEnumValue(String label) {
        if (label == null) return null;
        for (AnimalState state : AnimalState.values()) {
            if (getLabel(state).equals(label)) {
                return state;
            }
        }
        throw new IllegalArgumentException("No saved category with this label: " + label);
    }
}

