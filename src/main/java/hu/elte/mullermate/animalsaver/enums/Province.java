package hu.elte.mullermate.animalsaver.enums;

import java.util.Arrays;
import java.util.List;

import static java.util.stream.Collectors.toList;

/**
 * @author mullerm
 */
public enum Province {
    BACS_KISKUN("Bács-Kiskun megye"),
    BARANYA("Baranya megye"),
    BEKES("Békés megye"),
    BORSOD_ABAUJ_ZEMPLEN("Borsod-Abaúj-Zemplén megye"),
    CSONGRAD_CSANAD("Csongrád-Csanád megye"),
    FEJER("Fejér megye"),
    GYOR_MOSON_SOPRON("Győr-Moson-Sopron megye"),
    HAJDU_BIHAR("Hajdú-Bihar megye"),
    HEVES("Heves megye"),
    JASZ_NAGYKUN_SZOLNOK("Jász-Nagykun-Szolnok megye"),
    KOMAROM_ESZTERGOM("Komárom-Esztergom megye"),
    NOGRAD("Nógrád megye"),
    PEST("Pest megye"),
    SOMOGY("Somogy megye"),
    SZABOLCS_SZATMAR_BEREG("Szabolcs-Szatmár-Bereg megye"),
    TOLNA("Tolna megye"),
    VAS("Vas megye"),
    VESZPREM("Veszprém megye"),
    ZALA("Zala megye");

    public final String label;

    Province(String label) {
        this.label = label;
    }

    public static List<String> labelValues() {
        return Arrays.stream(values())
                .map(r -> r.label)
                .collect(toList());
    }
}
