package hu.elte.mullermate.animalsaver.enums;


import java.util.Arrays;
import java.util.List;

import static java.util.stream.Collectors.toList;

/**
 * @author mullerm
 */
public enum UserType {
    FINDER("Bejelentő"),
    RESCUER("Állatmentő"),
    ADMIN("Admin");

    private final String label;

    UserType(String label) {
        this.label = label;
    }

    public static String getLabel(UserType userType) {
        return userType.label;
    }

    public static List<String> labelValues() {
        return Arrays.stream(values())
                .map(r -> r.label)
                .collect(toList());
    }
}

