package hu.elte.mullermate.animalsaver.repository;

import hu.elte.mullermate.animalsaver.domain.AnimalHospital;
import org.springframework.data.jpa.repository.JpaRepository;


/**
 * @author mullerm
 */
public interface AnimalHospitalRepository extends JpaRepository<AnimalHospital, Long> {

}
