package hu.elte.mullermate.animalsaver.repository;

import hu.elte.mullermate.animalsaver.domain.Animal;
import hu.elte.mullermate.animalsaver.enums.AnimalState;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


/**
 * @author mullerm
 */
public interface AnimalRepository extends JpaRepository<Animal, Long> {

    /**
     * method to find Animals by their state
     *
     * @param state to search
     * @return animalList
     */
    List<Animal> findByState(AnimalState state);
}
