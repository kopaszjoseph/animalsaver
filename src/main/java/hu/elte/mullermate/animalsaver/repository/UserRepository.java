package hu.elte.mullermate.animalsaver.repository;

import hu.elte.mullermate.animalsaver.domain.User;
import hu.elte.mullermate.animalsaver.enums.UserType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

/**
 * @author mullerm
 */
public interface UserRepository extends JpaRepository<User, Long> {

    /**
     * method to find user by email
     *
     * @param email to search
     * @return Optional user
     */
    Optional<User> findByEmail(String email);

    /**
     * method to find users by their type
     *
     * @param type to search
     * @return list of users
     */
    List<User> findByType(UserType type);
}
