package hu.elte.mullermate.animalsaver.security;

import hu.elte.mullermate.animalsaver.domain.User;
import hu.elte.mullermate.animalsaver.repository.UserRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;


/**
 * @author mullerm
 */
public class MyUserDetailsService implements UserDetailsService {

    private final UserRepository repository;

    public MyUserDetailsService(UserRepository repository) {
        this.repository = repository;
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        User found = repository.findByEmail(email).orElseThrow(() -> new IllegalArgumentException("User with: " + email + " is not found."));
            return new MyUserDetails(found);

    }
}
