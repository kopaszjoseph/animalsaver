package hu.elte.mullermate.animalsaver.service;

import hu.elte.mullermate.animalsaver.domain.AnimalHospital;

import java.util.List;


/**
 * @author mullerm
 */
public interface AnimalHospitalService {

    /**
     * method to add a new AnimalHospital
     * @param toAdd AnimalHospital
     */
    void add(AnimalHospital toAdd);


    /**
     * method to list all AnimalHospitals
     * @return animalHospitalList
     */
    List<AnimalHospital> showAll();


    /**
     * method to remove an existing AnimalHospital
     * @param toClose AnimalHospital
     */
    void close(AnimalHospital toClose);


    /**
     * method to filter the AnimalHospitals
     *
     * @param province toSearch
     * @param city     toSearch
     * @param name     toSearch
     * @return found AnimalHospitals
     */
    List<AnimalHospital> allFilters(String province, String city, String name);


    /**
     * method to remove all added AnimalHospitals
     */
    void deleteAll();

}
