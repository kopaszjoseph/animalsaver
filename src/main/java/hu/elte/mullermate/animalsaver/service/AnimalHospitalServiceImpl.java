package hu.elte.mullermate.animalsaver.service;

import hu.elte.mullermate.animalsaver.domain.AnimalHospital;
import hu.elte.mullermate.animalsaver.repository.AnimalHospitalRepository;
import lombok.extern.log4j.Log4j2;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;
import java.util.stream.Collectors;


/**
 * @author mullerm
 */

@Log4j2
public class AnimalHospitalServiceImpl implements AnimalHospitalService {

    private final AnimalHospitalRepository animalHospitalRepository;

    public AnimalHospitalServiceImpl(AnimalHospitalRepository animalHospitalRepository) {
        this.animalHospitalRepository = animalHospitalRepository;
        log.info("AnimalHospitalService built.");
    }

    @Override
    public void add(AnimalHospital toAdd) {
        Objects.requireNonNull(toAdd, "Can't add null AnimalHospital.");
        AnimalHospital saved = animalHospitalRepository.save(toAdd);
        log.info("New AnimalHospital added by ADMIN: " + saved);
    }

    @Override
    public List<AnimalHospital> showAll() {
        log.info("All AnimalHospitals have been listed.");
        return animalHospitalRepository.findAll();
    }

    @Override
    public void close(AnimalHospital toClose) {
        log.info("AnimalHospital has been deleted: " + toClose);
        animalHospitalRepository.delete(toClose);
    }

    @Override
    public List<AnimalHospital> allFilters(String province, String city, String name) {
        List<Predicate<AnimalHospital>> allPredicates = new ArrayList<>();
        allPredicates.add(animalHospital -> animalHospital.getProvince().equals(province) || province == null);
        allPredicates.add(animalHospital -> animalHospital.getCity().toLowerCase().contains(city.toLowerCase()) || city.isEmpty());
        allPredicates.add(animalHospital -> animalHospital.getName().toLowerCase().contains(name.toLowerCase()) || name.isEmpty());
        log.info("Searching started in " + province + " province, " + city + " city, for " + name);
        return animalHospitalRepository.findAll().stream().filter(allPredicates.stream().reduce(x -> true, Predicate::and)).collect(Collectors.toList());
    }

    @Override
    public void deleteAll(){
        animalHospitalRepository.deleteAll();
        log.info("All AnimalHospitals were deleted by ADMIN.");
    }
}
