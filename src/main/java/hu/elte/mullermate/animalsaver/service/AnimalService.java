package hu.elte.mullermate.animalsaver.service;

import hu.elte.mullermate.animalsaver.domain.Animal;
import hu.elte.mullermate.animalsaver.domain.User;
import hu.elte.mullermate.animalsaver.enums.AnimalCategory;
import hu.elte.mullermate.animalsaver.enums.AnimalState;

import java.util.List;


/**
 * @author mullerm
 */
public interface AnimalService {


    /**
     * method to change the status of the Animal to the given state
     *
     * @param toChange Animal
     * @param state    given
     */
    void changeState(Animal toChange, AnimalState state);


    /**
     * method to list all of the Animals
     * @return animalList
     */
    List<Animal> showAll();


    /**
     * method to report a found Animal
     *
     * @param reporting user who found the animal
     * @param animal    toReport
     */
    void reportAnimal(User reporting, Animal animal);


    /**
     * method to modify the status of the Animal if it was under care
     * @param healing user who cares the found animal
     * @param animal toHeal
     */
    void careAnimal(User healing, Animal animal);


    /**
     * method to modify the status of the Animal if it was healed
     * @param releasing user who's releasing the healed animal
     * @param animal toRelease
     */
    void releaseAnimal(User releasing, Animal animal);


    /**
     * method to modify the status of the Animal if it was died
     * @param burying user who healed the animal before it's died
     * @param animal toBury
     */
    void buryAnimal(User burying, Animal animal);


    /**
     * method to show the number of released Animals by the User
     * @param toCount user
     * @return number of released Animals
     */
    Double releasedAnimals(User toCount);


    /**
     * method to show the number of dead Animals by the User
     * @param toCount user
     * @return number of dead Animals
     */
    Double deadAnimals(User toCount);


    /**
     * method to show the number of cared Animals by the User
     * @param toCount user
     * @return number of cared Animals
     */
    Double caredAnimals(User toCount);


    /**
     * method to show the number of reported animals by the User
     * @param toCount user
     * @return number of reported animals
     */
    Double reportedAnimals(User toCount);


    /**
     * method to show reported Animals from the database
     * @return animalList
     */
    List<Animal> showReportedAnimals();


    /**
     * method to show Animals healing by User
     * @param user toList
     * @return animalList
     */
    List<Animal> healingAnimalsByUser(User user);


    /**
     * method to show the Animals were healed by User
     * @param user toList
     * @return animalList
     */
    List<Animal> healedAnimalsByUser(User user);


    /**
     * method to reset the Animal database
     */
    void deleteAllAnimals();


    /**
     * method to change the Animal's damaged status
     * @param animal toChange
     */
    void changeDamagedStatus(Animal animal);


    /**
     * method to filter the Animals by province, city, state
     *
     * @param province the province to search
     * @param city     the city to search
     * @param state    the state of the animal
     * @param category the category of the animal
     * @return animalList
     */
    List<Animal> allFilters(String province, String city, AnimalState state, AnimalCategory category);
}

