package hu.elte.mullermate.animalsaver.service;

import hu.elte.mullermate.animalsaver.domain.Animal;
import hu.elte.mullermate.animalsaver.domain.User;
import hu.elte.mullermate.animalsaver.enums.AnimalCategory;
import hu.elte.mullermate.animalsaver.enums.AnimalState;
import hu.elte.mullermate.animalsaver.enums.UserType;
import hu.elte.mullermate.animalsaver.repository.AnimalRepository;
import hu.elte.mullermate.animalsaver.repository.UserRepository;
import lombok.extern.log4j.Log4j2;

import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;


/**
 * @author mullerm
 */
@Log4j2
public class AnimalServiceImpl implements AnimalService {

    private final AnimalRepository animalRepository;
    private final UserRepository userRepository;


    public AnimalServiceImpl(AnimalRepository animalRepository, UserRepository userRepository) {
        this.animalRepository = animalRepository;
        this.userRepository = userRepository;

        log.info("AnimalService built.");
    }


    @Override
    public void changeState(Animal toChange, AnimalState state){
        Objects.requireNonNull(toChange, "Animal can't be null.");
        Objects.requireNonNull(state, "State can't be null.");

        if(!toChange.getState().equals(state)) {
            toChange.setState(state);
            animalRepository.save(toChange);

            log.info("Animal's state has been changed: " + state.toString());
        } else {
            throw new IllegalArgumentException("Can't modify to the same state.");
        }
    }

    @Override
    public List<Animal> showAll() {

        log.info("All Animals have been listed.");

        return animalRepository.findAll();
    }


    @Override
    public List<Animal> allFilters(String province, String city, AnimalState state, AnimalCategory category) {
        List<Predicate<Animal>> allPredicates = new ArrayList<>();
        allPredicates.add(animal -> province == null || animal.getProvince().equals(province) || province.isEmpty());
        allPredicates.add(animal -> animal.getCity().toLowerCase().contains(city.toLowerCase()) || city.isEmpty());
        allPredicates.add(animal -> animal.getState().equals(state) || state == null);
        allPredicates.add(animal -> animal.getCategory().equals(category) || category == null);

        log.info("Searching started for filters...");

        return animalRepository.findAll().stream().filter(allPredicates.stream().reduce(x -> true, Predicate::and)).collect(Collectors.toList());
    }

    @Override
    public void reportAnimal(User reporting, Animal animal){
        User found = userRepository.findByEmail(reporting.getEmail()).orElseThrow(() ->
                new IllegalArgumentException("User with: " + reporting.getEmail() + " is not found."));

        manageAnimal(found, animal, AnimalState.FOUND);
    }

    @Override
    public void careAnimal(User healing, Animal animal){
        User found = userRepository.findByEmail(healing.getEmail()).orElseThrow(() ->
                new IllegalArgumentException("User with: " + healing.getEmail() + " is not found."));
        Optional<Animal> optionalAnimal = animalRepository.findById(animal.getId());

        if(optionalAnimal.isPresent()){
            Animal entity = optionalAnimal.get();
            manageAnimal(found, entity, AnimalState.CARE);
        }
    }

    @Override
    public void releaseAnimal(User releasing, Animal animal){
        User found = userRepository.findByEmail(releasing.getEmail()).orElseThrow(() ->
                new IllegalArgumentException("User with: " + releasing.getEmail() + " is not found."));
        Optional<Animal> optionalAnimal = animalRepository.findById(animal.getId());

        if(optionalAnimal.isPresent()){
            Animal entity = optionalAnimal.get();
            manageAnimal(found, entity, AnimalState.RELEASED);
        }
    }

    @Override
    public void buryAnimal(User burying, Animal animal){
        User found = userRepository.findByEmail(burying.getEmail()).orElseThrow(() ->
                new IllegalArgumentException("User with: " + burying.getEmail() + " is not found."));
        Optional<Animal> optionalAnimal = animalRepository.findById(animal.getId());

        if(optionalAnimal.isPresent()){
            Animal entity = optionalAnimal.get();
            manageAnimal(found, entity, AnimalState.DEAD);
        }
    }

    private void manageAnimal(User manager, Animal animal, AnimalState state){
        Objects.requireNonNull(manager, "User can't be null.");
        Objects.requireNonNull(animal, "Animal can't be null.");
        Objects.requireNonNull(state, "State can't be null.");
        switch (state){
            case FOUND:
                if (checkUserType(manager, UserType.FINDER) || checkUserType(manager, UserType.RESCUER) || checkUserType(manager, UserType.ADMIN)) {
                    changeState(animal, state);
                    animal.setFinder(manager);
                    manager.getAnimals().add(animal);
                    manager.setReportedCount(reportedAnimals(manager) + 1);

                    userRepository.save(manager);
                    animalRepository.save(animal);

                    log.info(manager.getEmail() + ": has reported a new Animal: " + animal);
                } else {
                    throw new IllegalArgumentException("Wrong UserType!");
                }
                break;

            case CARE:
                if (checkUserType(manager, UserType.RESCUER) || checkUserType(manager, UserType.ADMIN)) {
                    animal.setFinder(manager);
                    manager.getAnimals().add(animal);
                    changeState(animal, state);
                    manager.setCaredCount(caredAnimals(manager) + 1);

                    userRepository.save(manager);
                    animalRepository.save(animal);

                    log.info(manager.getEmail() + ": will care about: " + animal);
                } else {
                    throw new IllegalArgumentException("Wrong UserType!");
                }
                break;

            case RELEASED:
                if (checkUserType(manager, UserType.RESCUER) || checkUserType(manager, UserType.ADMIN)) {
                    changeState(animal, state);
                    manager.setReleasedCount(releasedAnimals(manager) + 1);

                    userRepository.save(manager);
                    animalRepository.save(animal);

                    log.info(manager.getEmail() + ": released: " + animal);
                } else {
                    throw new IllegalArgumentException("Wrong UserType!");
                }
                break;

            case DEAD:
                if (checkUserType(manager, UserType.RESCUER) || checkUserType(manager, UserType.ADMIN)) {
                    changeState(animal, state);
                    manager.setDeadCount(deadAnimals(manager) + 1);

                    userRepository.save(manager);
                    animalRepository.save(animal);

                    log.info(manager.getEmail() + ": buried: " + animal);
                } else {
                    throw new IllegalArgumentException("Wrong UserType!");
                }
                break;
        }
    }

    private boolean checkUserType(User user, UserType type) {
        return user.getType().equals(type);
    }

    @Override
    public Double releasedAnimals(User toCount){
        if (checkUserType(toCount, UserType.RESCUER)) {
            User found = userRepository.findByEmail(toCount.getEmail()).orElseThrow(() ->
                    new IllegalArgumentException("User with: " + toCount.getEmail() + " is not found."));

            log.info("Released animals for " + found.getName() + ": " + found.getReleasedCount());

            return found.getReleasedCount();
        } else {
            log.info("UserType is incorrect.");

            throw new IllegalArgumentException("Wrong UserType!");
        }
    }

    @Override
    public Double deadAnimals(User toCount){
        if (checkUserType(toCount, UserType.RESCUER)) {
            User found = userRepository.findByEmail(toCount.getEmail()).orElseThrow(() ->
                    new IllegalArgumentException("User with: " + toCount.getEmail() + " is not found."));

            log.info("Buried animals for " + found.getName() + ": " + found.getDeadCount());

            return found.getDeadCount();
        } else {
            log.info("UserType is incorrect.");

            throw new IllegalArgumentException("Wrong UserType!");
        }
    }

    @Override
    public Double caredAnimals(User toCount){
        if (checkUserType(toCount, UserType.RESCUER)) {
            User found = userRepository.findByEmail(toCount.getEmail()).orElseThrow(() ->
                    new IllegalArgumentException("User with: " + toCount.getEmail() + " is not found."));

            log.info("Cared animals for " + found.getName() + ": " + found.getCaredCount());

            return found.getCaredCount();
        } else {
            log.info("UserType is incorrect.");

            throw new IllegalArgumentException("Wrong UserType!");
        }
    }

    @Override
    public Double reportedAnimals(User toCount){
        if (checkUserType(toCount, UserType.FINDER) || checkUserType(toCount, UserType.RESCUER)) {
            User found = userRepository.findByEmail(toCount.getEmail()).orElseThrow(() ->
                    new IllegalArgumentException("User with: " + toCount.getEmail() + " is not found."));

            log.info("Reported animals for " + found.getName() + ": " + found.getReportedCount());

            return found.getReportedCount();
        } else {
            log.info("UserType is incorrect.");

            throw new IllegalArgumentException("Wrong UserType!");
        }
    }

    @Override
    public List<Animal> showReportedAnimals(){

        log.info("Reported Animals have been listed.");

        return animalRepository.findByState(AnimalState.FOUND);
    }

    @Override
    public List<Animal> healingAnimalsByUser(User user) {
        User toList = userRepository.findByEmail(user.getEmail()).orElseThrow(() ->
                new IllegalArgumentException("User with: " + user.getEmail() + " is not found."));
        List<Animal> found = toList.getAnimals();

        if (checkUserType(toList, UserType.RESCUER) || checkUserType(toList, UserType.ADMIN)) {
            List<Animal> ret = found.stream().filter(animal -> animal.getState().equals(AnimalState.CARE)).collect(Collectors.toList());

            log.info("Animals healing by " + user.getName() + " have been listed");

            return ret;
        } else {
            log.info("Animals healing by " + user.getName() + " is empty.");

            return Collections.emptyList();
        }
    }

    @Override
    public List<Animal> healedAnimalsByUser(User user) {
        User toList = userRepository.findByEmail(user.getEmail()).orElseThrow(() ->
                new IllegalArgumentException("User with: " + user.getEmail() + " is not found."));
        List<Animal> found = toList.getAnimals();

        log.info("Animal was healed by " + user.getName() + " have been listed.");

        return found.stream().filter(animal -> animal.getState().equals(AnimalState.RELEASED) ||
                animal.getState().equals(AnimalState.DEAD)).collect(Collectors.toList());
    }

    @Override
    public void changeDamagedStatus(Animal animal){
        Optional<Animal> optFound = animalRepository.findById(animal.getId());
        optFound.ifPresent(value -> {
            value.setDamaged(!value.isDamaged());

            animalRepository.save(value);

            log.info("Damaged status has been modified for: " + value);
        });
    }

    @Override
    public void deleteAllAnimals() {
        animalRepository.deleteAll();

        log.info("All Animals have been deleted by ADMIN");
    }
}
