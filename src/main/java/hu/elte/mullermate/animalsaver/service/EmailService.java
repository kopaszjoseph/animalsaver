package hu.elte.mullermate.animalsaver.service;

import hu.elte.mullermate.animalsaver.domain.Animal;
import hu.elte.mullermate.animalsaver.domain.User;


/**
 * @author mullerm
 */
public interface EmailService {


    /**
     * method to send welcome message
     *
     * @param user to send email
     */
    void sendRegistrationMessage(User user);

    /**
     * method to send a successful reporting message
     *
     * @param user to send email
     */
    void sendReportMessage(User user);

    /**
     * method to send
     *
     * @param healing rescuer
     * @param animal  cared
     */
    void sendCareMessage(User healing, Animal animal);

    /**
     * method to send custom message for a rescuer
     *
     * @param from logged in user
     * @param to   other user
     * @param text email content
     */
    void sendCustomMessage(User from, User to, String text);

    /**
     * method to send care message if finder's account has been deleted
     *
     * @param rescuer user
     * @param animal  cared
     */
    void sendDeletedFinderCareMessage(User rescuer, Animal animal);

    /**
     * method to send forgotten password message
     *
     * @param user who forgot his password
     */
    void sendForgottenPasswordMessage(User user);
}
