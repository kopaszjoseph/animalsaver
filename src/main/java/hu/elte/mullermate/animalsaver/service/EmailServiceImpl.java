package hu.elte.mullermate.animalsaver.service;

import hu.elte.mullermate.animalsaver.domain.Animal;
import hu.elte.mullermate.animalsaver.domain.User;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;


/**
 * @author mullerm
 */
@Log4j2
public class EmailServiceImpl implements EmailService {

    @Value("${spring.mail.username}")
    private String MESSAGE_FROM;

    private final JavaMailSender javaMailSender;


    public EmailServiceImpl(JavaMailSender javaMailSender){
        this.javaMailSender = javaMailSender;
        log.info("EmailService built.");
    }

    @Override
    public void sendRegistrationMessage(User user){
        SimpleMailMessage registerMessage;
        try{
            registerMessage = new SimpleMailMessage();
            registerMessage.setFrom(MESSAGE_FROM);
            registerMessage.setTo(user.getEmail());
            registerMessage.setSubject("AnimalSaver - Sikeres regisztráció");
            registerMessage.setText("Kedves " + user.getName() + "!\n\nSikeresen regisztráltál a AnimalSaver oldalra!" +
                    "\nReméljük, hogy " + user.getProvince().substring(0, user.getProvince().length() - 1) + "ében minél több állatot sikerül megmentened." +
                    "\n\nÜdv," +
                    "\nAnimalSaver");

            javaMailSender.send(registerMessage);

            log.info("Email was sent about registration to: " + user.getEmail());
        } catch (MailException e){
            log.error("An error occurred while sending register message: " + user.getEmail());
        } catch (IllegalArgumentException e){
            log.error("User with this email " + user.getEmail() + " is not found.");
        }
    }

    @Override
    public void sendReportMessage(User user){
        SimpleMailMessage reportMessage;
        try{
            reportMessage = new SimpleMailMessage();
            reportMessage.setFrom(MESSAGE_FROM);
            reportMessage.setTo(user.getEmail());
            reportMessage.setSubject("AnimalSaver - Bejelentettél egy állatot");
            reportMessage.setText("Kedves " + user.getName() + "! \n\nSikeresen bejelentettél egy állatot!" +
                    "\nKöszönjük!" +
                    "\n\nÜdv," +
                    "\nAnimalSaver");

            javaMailSender.send(reportMessage);

            log.info("Email was sent about reporting to: " + user.getEmail());
        } catch (MailException e){
            log.error("An error occurred while sending report message: " + user.getEmail());
        } catch (IllegalArgumentException e){
            log.error("User with this email " + user.getEmail() + " is not found.");
        }

    }

    @Override
    public void sendCareMessage(User user, Animal animal){
        SimpleMailMessage rescuerCareMessage;
        SimpleMailMessage finderCareMessage;
        try {
            User reporter = animal.getFinder();
            rescuerCareMessage = new SimpleMailMessage();
            rescuerCareMessage.setFrom(MESSAGE_FROM);
            rescuerCareMessage.setTo(user.getEmail());
            rescuerCareMessage.setSubject("AnimalSaver - Szárnyaid alá vettél egy állatot!");
            rescuerCareMessage.setText("Kedves " + user.getName() + "! \n\nSikeresen gondozni kezdtél egy állatot, " +
                    "ehhez először is vedd fel a kapcsolatot a bejelentővel!\n\n" +
                    reporter.toContact() +
                    "\nAz állatot itt találta: " + animal.foundAddress() +
                    "\nBeszéljétek meg a találkozás részleteit és legjobb tudásod szerint kezdd el gondozni az állatot. " +
                    "Remélhetőleg meggyógyul és szabadon tudod majd engedni." +
                    "\n\nÜdv," +
                    "\nAnimalSaver");

            javaMailSender.send(rescuerCareMessage);

            log.info("Email was sent about care to: " + user.getEmail() + " rescuer.");

            finderCareMessage = new SimpleMailMessage();
            finderCareMessage.setFrom(MESSAGE_FROM);
            finderCareMessage.setTo(reporter.getEmail());
            finderCareMessage.setSubject("AnimalSaver - Kapcsolatteremtés");
            finderCareMessage.setText("Kedves " + reporter.getName() + "! \n\nEgy általad bejelentett állatot " + user.getName()
                    + " gondozni szeretne, ezért valószínű keresni fog az elérhetőségeid valamelyikén.\n\n" +
                    "Amennyiben mégsem keresne, te is felveheted vele a kapcsolatot.\n\n" + user.toContact() +
                    "\nAz állatot itt találtad: " + animal.foundAddress() +
                    "\n\nÜdv," +
                    "\nAnimalSaver");

            javaMailSender.send(finderCareMessage);

            log.info("Email was sent about care to: " + reporter.getEmail() + " finder.");
        } catch (MailException e){
            log.error("An error occurred while sending care message: " + user.getEmail());
        } catch (IllegalArgumentException e){
            log.error("User with this email " + user.getEmail() + " is not found.");
        } catch (NullPointerException e){
            log.error("Failed to send email, reporter's account is deleted.");
        }
    }

    @Override
    public void sendDeletedFinderCareMessage(User rescuer, Animal animal){
        SimpleMailMessage deletedFinderCareMessage;
        try{
            deletedFinderCareMessage = new SimpleMailMessage();
            deletedFinderCareMessage.setFrom(MESSAGE_FROM);
            deletedFinderCareMessage.setTo(rescuer.getEmail());
            deletedFinderCareMessage.setSubject("AnimalSaver - Szárnyaid alá vettél egy állatot!");
            deletedFinderCareMessage.setText("Kedves " + rescuer.getName() + "!\n\nSajnos a bejelentő törölte a fiókját, ezért " +
                    "próbáld meg felvenni vele a kapcsolatot telefonon!\n\n" + "Telefonszám: " + animal.getFinderPhone() +
                    "\nAz állatot itt találta: " + animal.foundAddress() +
                    "\n\nÜdv," +
                    "\nAnimalSaver");

            javaMailSender.send(deletedFinderCareMessage);

            log.info("Email was sent about care to: " + rescuer.getEmail() + " rescuer.");
        } catch (MailException e) {
            log.error("An error occurred while sending care message: " + rescuer.getEmail());
        } catch (IllegalArgumentException e) {
            log.error("User with this email " + rescuer.getEmail() + " is not found.");
        }
    }

    @Override
    public void sendForgottenPasswordMessage(User user) {
        SimpleMailMessage forgottenMessage;
        try {
            forgottenMessage = new SimpleMailMessage();
            forgottenMessage.setFrom(MESSAGE_FROM);
            forgottenMessage.setTo(user.getEmail());
            forgottenMessage.setSubject("AnimalSaver - Elfelejtett jelszó");
            forgottenMessage.setText("Kedves " + user.getName() + "! \n\nÚgy tűnik, elfelejtetted a jelszavad." +
                    "\nIdeiglenesen beállítottunk Neked egy új jelszót, de kérjük ezt minél hamarabb változtasd meg!" +
                    "\n\nAz új jelszó:" +
                    "\nAnimalSaverApplication2020" +
                    "\n\nÜdv,\nAnimalSaver");

            javaMailSender.send(forgottenMessage);

            log.info("Forgotten password email was sent to: " + user.getEmail());
        } catch (MailException e) {
            log.error("An error occurred while sending forgotten password message: " + user.getEmail());
        } catch (IllegalArgumentException e) {
            log.error("User with this email " + user.getEmail() + " is not found.");
        }
    }

    @Override
    public void sendCustomMessage(User from, User to, String text) {
        SimpleMailMessage customMessage;
        try {
            customMessage = new SimpleMailMessage();
            customMessage.setFrom(MESSAGE_FROM);
            customMessage.setTo(to.getEmail());
            customMessage.setSubject("AnimalSaver - Üzeneted érkezett!");
            customMessage.setText("Kedves " + to.getName() + "! \n\nÜzeneted érkezett " + from.getEmail() + " felhasználótól. \n\n\n" + text +
                    "\n\nÜdv," +
                    "\nAnimalSaver");

            javaMailSender.send(customMessage);

            log.info("Custom email was sent to: " + to.getEmail());
        } catch (MailException e){
            log.error("An error occurred while sending care message: " + to.getEmail());
        } catch (IllegalArgumentException e){
            log.error("User with this email " + to.getEmail() + " is not found.");
        }
    }

}
