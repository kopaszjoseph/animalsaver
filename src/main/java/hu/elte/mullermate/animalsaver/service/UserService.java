package hu.elte.mullermate.animalsaver.service;

import hu.elte.mullermate.animalsaver.domain.User;
import hu.elte.mullermate.animalsaver.enums.UserType;

import java.util.List;


/**
 * @author mullerm
 */
public interface UserService {

    /**
     * method to find a user
     *
     * @param email to search
     * @return user
     */
    User findByEmail(String email);


    /**
     * method to save a user
     * @param user toSave
     * @return user
     */
    User add(User user);


    /**
     * method to get the number of Animals reported by User
     * @param user toCount
     * @return number of reported animals
     */
    double getReportedCount(User user);


    /**
     * method to get the number of Animals cared by User
     * @param user toCount
     * @return number of cared animals
     */
    double getCaredCount(User user);


    /**
     * method to get the number of Animals buried by User
     * @param user toCount
     * @return number of buried animals
     */
    double getDeadCount(User user);


    /**
     * method to get the number of Animals released by User
     * @param user toCount
     * @return number of released animals
     */
    double getReleasedCount(User user);


    /**
     * method to update User info
     * @param oldInfo user to update
     * @param newInfo info to be saved
     */
    void updateUser(User oldInfo, User newInfo);


    /**
     * method to delete an account after a password check
     * @param toDel user
     * @param password check
     */
    void deleteAccount(User toDel, String password);


    /**
     * method to delete an account by ADMIN
     *
     * @param toDel user
     */
    void adminDeleteAccount(User toDel);

    /**
     * method to delete all accounts by ADMIN
     */
    void adminDeleteAllUsers();

    /**
     * method to show registered users by their type
     *
     * @param type FINDER/RESCUER
     * @return userList
     */
    List<User> showUsers(UserType type);


    /**
     * method to calculate the survival rate
     * @param toRate user
     * @return rating
     */
    double rating(User toRate);


    /**
     * method to list all of the Users
     * @return userList
     */
    List<User> findAll();


    /**
     * method to get rescuers by filters
     *
     * @param name to search
     * @param province to search
     * @return user
     */
    List<User> allFilters(String name, String province);


    /**
     * method to reset all users statistics
     */
    void resetAllUsersStatistics();


    /**
     * method to update the password of the User
     *
     * @param old   user
     * @param fresh user
     */
    void updatePassword(User old, User fresh);

}

