package hu.elte.mullermate.animalsaver.service;

import hu.elte.mullermate.animalsaver.domain.User;
import hu.elte.mullermate.animalsaver.enums.UserType;
import hu.elte.mullermate.animalsaver.repository.AnimalRepository;
import hu.elte.mullermate.animalsaver.repository.UserRepository;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.BeanUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;


/**
 * @author mullerm
 */
@Log4j2
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final AnimalRepository animalRepository;
    private final EmailService emailService;
    private final PasswordEncoder passwordEncoder;


    public UserServiceImpl(UserRepository userRepository, AnimalRepository animalRepository, EmailService emailService, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.animalRepository = animalRepository;
        this.emailService = emailService;
        this.passwordEncoder = passwordEncoder;

        log.info("UserService built.");
    }


    @Override
    public User add(User user){
        Assert.notNull(user, "User can't be null!");
        if(!isRegisteredEmail(user.getEmail())) {
            user.setPassword(passwordEncoder.encode(user.getPassword()));
            user.setAuthority("ROLE_" + user.getType().toString());
            User toSave = userRepository.save(user);
            new Thread(() -> emailService.sendRegistrationMessage(toSave)).start();

            log.info("New User added: " + toSave);

            return toSave;
        } else {
            log.info("This email is already registered: " + user.getEmail());

            throw new IllegalArgumentException("This email is already registered: " + user.getEmail());
        }
    }

    private boolean isRegisteredEmail(String email) {
        List<User> users = userRepository.findAll();
        for (User u : users) {
            if (u.getEmail().equals(email)) {
                log.warn("Somebody already registered with: " + email);

                return true;
            }
        }
        return false;
    }

    @Override
    public double getReportedCount(User user) {
        User found = userRepository.findByEmail(user.getEmail()).orElseThrow(() ->
                new IllegalArgumentException("User with: " + user.getEmail() + " is not found."));

        log.info("Reported animals for " + found.getName() + ": " + found.getReportedCount());

        return found.getReportedCount();
    }

    @Override
    public double getCaredCount(User user) {
        User found = userRepository.findByEmail(user.getEmail()).orElseThrow(() ->
                new IllegalArgumentException("User with: " + user.getEmail() + " is not found."));

        log.info("Cared animals for " + found.getName() + ": " + found.getCaredCount());

        return found.getCaredCount();
    }

    @Override
    public double getDeadCount(User user) {
        User found = userRepository.findByEmail(user.getEmail()).orElseThrow(() ->
                new IllegalArgumentException("User with: " + user.getEmail() + " is not found."));

        log.info("Buried animals for " + found.getName() + ": " + found.getDeadCount());

        return found.getDeadCount();
    }

    @Override
    public double getReleasedCount(User user) {
        User found = userRepository.findByEmail(user.getEmail()).orElseThrow(() ->
                new IllegalArgumentException("User with: " + user.getEmail() + " is not found."));

        log.info("Released animals for " + found.getName() + ": " + found.getReleasedCount());

        return found.getReleasedCount();
    }

    @Override
    public User findByEmail(String email) {
        log.info("Searching started for: " + email);

        return userRepository.findByEmail(email).orElseThrow(() ->
                new IllegalArgumentException("User with: " + email + " is not found."));
    }

    @Override
    public void updateUser(User oldInfo, User newInfo) {
        User found = userRepository.findByEmail(oldInfo.getEmail()).orElseThrow(() ->
                new IllegalArgumentException("User with: " + oldInfo.getEmail() + " is not found."));
        BeanUtils.copyProperties(newInfo, found, "id", "email", "password", "authority", "type", "animals", "caredCount", "reportedCount", "releasedCount", "deadCount");

        userRepository.save(found);

        log.info("User info has been updated: " + found);
    }

    @Override
    public void updatePassword(User old, User fresh) {
        User found = userRepository.findByEmail(old.getEmail()).orElseThrow(() ->
                new IllegalArgumentException("User with: " + old.getEmail() + " is not found."));
        found.setPassword(passwordEncoder.encode(fresh.getPassword()));

        userRepository.save(found);

        log.info("User's password has been updated.");
    }

    @Override
    public void deleteAccount(User toDel, String password) {
        User found = userRepository.findByEmail(toDel.getEmail()).orElseThrow(() ->
                new IllegalArgumentException("User with: " + toDel.getEmail() + " is not found."));
        if (passwordEncoder.matches(password, found.getPassword())) {
            setAnimalDetailsForDeletedUser(found);

            log.info("Account was deleted: " + found);
        } else {
            throw new IllegalArgumentException("Wrong password!");
        }
    }

    private void setAnimalDetailsForDeletedUser(User found) {
        if (found.getType().equals(UserType.ADMIN)) {
            return;
        }
        found.getAnimals().forEach(animal -> {
            animal.setFinderName("törölt fiók");
            animal.setFinderPhone(found.getPhone());
            animal.setFinder(null);
            animalRepository.save(animal);
        });
        found.getAnimals().clear();

        log.info("Account was deleted by ADMIN: " + found);

        userRepository.delete(found);
    }

    @Override
    public void adminDeleteAllUsers() {
        List<User> allUsers = userRepository.findAll();
        allUsers.forEach(this::setAnimalDetailsForDeletedUser);
    }

    @Override
    public void adminDeleteAccount(User toDel) {
        User found = userRepository.findByEmail(toDel.getEmail()).orElseThrow(() ->
                new IllegalArgumentException("User with: " + toDel.getEmail() + " is not found."));
        setAnimalDetailsForDeletedUser(found);
    }

    @Override
    public List<User> showUsers(UserType type) {
        log.info("All " + type + " users have been listed.");
        return userRepository.findByType(type);
    }

    @Override
    public double rating(User toRate){
        User found = userRepository.findByEmail(toRate.getEmail()).orElseThrow(() ->
                new IllegalArgumentException("User with: " + toRate.getEmail() + " is not found."));
        if (found.getType().equals(UserType.RESCUER) && found.getCaredCount() > 0) {
            double result = 100 - found.getDeadCount() / found.getCaredCount() * 100;

            log.info("Rating for " + toRate.getName() + ": " + result);

            return result;
        } else {
            return 0;
        }
    }

    @Override
    public List<User> findAll() {
        log.info("All Users have been listed.");

        return userRepository.findAll();
    }

    @Override
    public List<User> allFilters(String name, String province) {
        List<Predicate<User>> allPredicates = new ArrayList<>();
        allPredicates.add(user -> user.getName().toLowerCase().contains(name.toLowerCase()) || name.isEmpty());
        allPredicates.add(user -> user.getProvince().equals(province) || province == null);
        allPredicates.add(user -> user.getType().equals(UserType.RESCUER));

        log.info("Searching started for filters...");

        return userRepository.findAll().stream().filter(allPredicates.stream().reduce(x -> true, Predicate::and)).collect(Collectors.toList());
    }

    @Override
    public void resetAllUsersStatistics(){
        userRepository.findAll().forEach(user -> {
            user.setCaredCount(0);
            user.setReleasedCount(0);
            user.setReportedCount(0);
            user.setDeadCount(0);
            userRepository.save(user);
        });
        log.info("Users statistics have been reseted by ADMIN.");
    }

}

