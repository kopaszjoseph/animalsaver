package hu.elte.mullermate.animalsaver.view;

import com.vaadin.data.Binder;
import com.vaadin.data.Validator;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.shared.Position;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import hu.elte.mullermate.animalsaver.domain.AnimalHospital;
import hu.elte.mullermate.animalsaver.enums.Province;
import hu.elte.mullermate.animalsaver.service.AnimalHospitalService;
import org.springframework.context.annotation.Scope;

/**
 * @author mullerm
 */
@Scope("vaadin-ui")
@SpringView(name = AddHospitalView.VIEW_NAME)
public class AddHospitalView extends FormLayout implements View {

    public static final String VIEW_NAME = "addhospital";

    private AnimalHospitalService animalHospitalService;
    private Binder<AnimalHospital> binder;


    public AddHospitalView(AnimalHospitalService animalHospitalService){
        this.animalHospitalService = animalHospitalService;

        setHeightUndefined();
        setMargin(true);

        initHeader();
        initBinder();
    }

    private void initHeader(){
        Label header = new Label("Új állatkórház felvétele");
        header.setStyleName(ValoTheme.LABEL_H3);
        addComponent(header);
    }

    private void initBinder(){
        binder = new Binder<>();

        TextField nameField = new TextField("Intézmény neve:");
        nameField.focus();
        binder.forField(nameField).asRequired("Kötelező!").bind(AnimalHospital::getName, AnimalHospital::setName);
        nameField.setIcon(VaadinIcons.TEXT_LABEL);
        nameField.setWidth(300, Unit.PIXELS);

        ComboBox<String> provinceField = new ComboBox<>("Megye:");
        provinceField.setItems(Province.labelValues());
        binder.forField(provinceField).asRequired("Kötelező!").bind(AnimalHospital::getProvince, AnimalHospital::setProvince);
        provinceField.setIcon(VaadinIcons.MAP_MARKER);
        provinceField.setWidth(250, Unit.PIXELS);

        TextField cityField = new TextField("Város:");
        binder.forField(cityField).asRequired("Kötelező!").bind(AnimalHospital::getCity, AnimalHospital::setCity);
        cityField.setIcon(VaadinIcons.BUILDING);
        cityField.setWidth(250, Unit.PIXELS);

        TextField addressField = new TextField("Cím:");
        binder.forField(addressField).asRequired("Kötelező!").bind(AnimalHospital::getAddress, AnimalHospital::setAddress);
        addressField.setIcon(VaadinIcons.HOME);
        addressField.setWidth(250, Unit.PIXELS);

        TextField websiteField = new TextField("Honlap:");
        binder.forField(websiteField).bind(AnimalHospital::getWebsite, AnimalHospital::setWebsite);
        websiteField.setIcon(VaadinIcons.GLOBE_WIRE);
        websiteField.setWidth(250, Unit.PIXELS);

        TextField emailField = new TextField("E-mail:");
        binder.forField(emailField).asRequired("Kötelező!").bind(AnimalHospital::getEmail, AnimalHospital::setEmail);
        emailField.setIcon(VaadinIcons.AT);
        emailField.setWidth(250, Unit.PIXELS);

        TextField phoneField = new TextField("Telefonszám:");
        binder.forField(phoneField).asRequired("Kötelező!").bind(AnimalHospital::getPhone, AnimalHospital::setPhone);
        phoneField.setIcon(VaadinIcons.PHONE_LANDLINE);
        phoneField.setWidth(250, Unit.PIXELS);

        binder.withValidator(Validator.from(user -> !nameField.isEmpty()
                && !provinceField.isEmpty()
                && !cityField.isEmpty()
                && !addressField.isEmpty()
                && !emailField.isEmpty()
                && !phoneField.isEmpty(), "Az összes kötelező adatot meg kell adni!"));
        Label validationStatus = new Label();
        binder.setStatusLabel(validationStatus);

        Button addButton = new Button("Hozzáadás");
        addButton.setIcon(VaadinIcons.PLUS);
        addButton.setEnabled(false);
        addButton.setStyleName(ValoTheme.BUTTON_PRIMARY);
        addButton.addClickListener(event -> {
            try{
                animalHospitalService.add(binder.getBean());
                Notification.show("Sikeres hozzáadás!",
                        Notification.Type.TRAY_NOTIFICATION).setPosition(Position.TOP_LEFT);
                binder.setBean(new AnimalHospital());
            } catch (NullPointerException e){
                Notification.show("Null kórházat nem tudsz felvenni!", Notification.Type.ERROR_MESSAGE);
            }
        });
        binder.addStatusChangeListener(event -> addButton.setEnabled(binder.isValid()));
        addComponents(nameField, provinceField, cityField, addressField, websiteField,
                emailField, phoneField, validationStatus, addButton);
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent){
        getUI().getPage().setTitle("AnimalSaver - Állatkórház felvétele");
        binder.setBean(new AnimalHospital());
    }

}
