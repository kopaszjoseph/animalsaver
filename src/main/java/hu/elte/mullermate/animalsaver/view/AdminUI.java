package hu.elte.mullermate.animalsaver.view;

import com.vaadin.annotations.Theme;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.PushStateNavigation;
import com.vaadin.server.VaadinRequest;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.spring.navigator.SpringNavigator;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import hu.elte.mullermate.animalsaver.domain.User;
import hu.elte.mullermate.animalsaver.enums.UserType;
import hu.elte.mullermate.animalsaver.security.MyUserDetails;
import hu.elte.mullermate.animalsaver.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import java.text.DecimalFormat;

import static hu.elte.mullermate.animalsaver.enums.UserType.getLabel;

/**
 * @author mullerm
 */
@SpringUI
@Theme("valo")
@PushStateNavigation
public class AdminUI extends UI {

    @Autowired
    private UserService userService;

    private final DecimalFormat df = new DecimalFormat("##.##");
    public User user;
    private SpringNavigator navigator;
    private Panel viewContainer;
    private GridLayout baseLayout;

    private MenuBar menuBar;
    private MenuBar.MenuItem home;
    private MenuBar.MenuItem report;
    private MenuBar.MenuItem reported;
    private MenuBar.MenuItem care;
    private MenuBar.MenuItem healing;
    private MenuBar.MenuItem hospitals;
    private MenuBar.MenuItem rescuers;
    private MenuBar.MenuItem information;
    private MenuBar.MenuItem heal;
    private MenuBar.MenuItem account;
    private MenuBar.MenuItem login;
    private MenuBar.MenuItem register;
    private MenuBar.MenuItem edit;
    private MenuBar.MenuItem logout;
    private MenuBar.MenuItem admin;
    private MenuBar.MenuItem statistics;
    private MenuBar.MenuItem forgottenPassword;

    public AdminUI(SpringNavigator navigator){
        this.navigator = navigator;
        UI.setCurrent(this);
    }

    @Override
    protected void init(VaadinRequest vaadinRequest) {
        try {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            if(!(authentication instanceof AnonymousAuthenticationToken)){
                MyUserDetails principal = (MyUserDetails) authentication.getPrincipal();
                user = principal.getUser();
            }
        } catch (NullPointerException e){
            Notification.show("Jelentkezz be!", Notification.Type.ERROR_MESSAGE);
        }

        setUpGridLayout();
        restrictVisibility();

        navigator.init(this, viewContainer);
        setNavigator(navigator);
        navigator.addView("", HomeView.class);
        navigator.setErrorView(HomeView.class);
    }

    private void setUpGridLayout() {
        this.baseLayout = new GridLayout(1, 3);
        baseLayout.setSizeFull();
        baseLayout.setMargin(false);
        baseLayout.setSpacing(true);
        baseLayout.setRowExpandRatio(0, 0.01f);
        baseLayout.setRowExpandRatio(1, 0.01f);
        baseLayout.setRowExpandRatio(2, 0.98f);
        baseLayout.setDefaultComponentAlignment(Alignment.TOP_CENTER);
        addHeader();
        addMenuBar();
        addContentLayout();
        setContent(baseLayout);
    }

    private void addHeader(){
        Label header = new Label("AnimalSaver");
        header.addStyleName(ValoTheme.LABEL_H2);
        header.addStyleName(ValoTheme.LABEL_BOLD);
        header.setSizeUndefined();
        baseLayout.addComponent(header, 0, 0);
        baseLayout.setComponentAlignment(header, Alignment.MIDDLE_CENTER);
    }

    private void addMenuBar(){
        menuBar = new MenuBar();
        home = menuBar.addItem("Főoldal", VaadinIcons.HOME_O, menuItem -> getNavigator().navigateTo(""));
        report = menuBar.addItem("Bejelentés", VaadinIcons.CLIPBOARD_CROSS, menuItem -> getNavigator().navigateTo(ReportView.VIEW_NAME));
        reported = menuBar.addItem("Bejelentett állatok", VaadinIcons.RECORDS, menuItem -> getNavigator().navigateTo(ReportedView.VIEW_NAME));
        heal = menuBar.addItem("Állatmentés", VaadinIcons.DOCTOR, null);
        care = heal.addItem("Gondozás", menuItem -> getNavigator().navigateTo(CareView.VIEW_NAME));
        healing = heal.addItem("Gondozott", menuItem -> getNavigator().navigateTo(HealingView.VIEW_NAME));
        hospitals = menuBar.addItem("Állatkórházak", VaadinIcons.HOSPITAL, menuItem -> getNavigator().navigateTo(AnimalHospitalView.VIEW_NAME));
        rescuers = menuBar.addItem("Állatmentők", VaadinIcons.USERS, menuItem -> getNavigator().navigateTo(RescuersView.VIEW_NAME));
        information = menuBar.addItem("Információ", VaadinIcons.INFO_CIRCLE_O, menuItem -> getNavigator().navigateTo(InformationView.VIEW_NAME));
        admin = menuBar.addItem("ADMIN", VaadinIcons.USERS, menuItem -> getNavigator().navigateTo(AdminView.VIEW_NAME));
        account = menuBar.addItem("Saját fiók", VaadinIcons.USER, null);
        MenuBar.Command toLogin = (MenuBar.Command) menuItem -> {
            getUI().getSession().close();
            getUI().getPage().setLocation("/login");
        };
        statistics = account.addItem("Statisztikáim", VaadinIcons.INFO, menuItem -> showUserInfo());
        login = account.addItem("Bejelentkezés", VaadinIcons.SIGN_IN, toLogin);
        forgottenPassword = account.addItem("Elfelejtett jelszó", VaadinIcons.COGS, menuItem -> getNavigator().navigateTo(ForgottenPasswordView.VIEW_NAME));
        register = account.addItem("Regisztráció", VaadinIcons.KEY_O, menuItem -> getNavigator().navigateTo(RegistrationView.VIEW_NAME));
        edit = account.addItem("Szerkesztés", VaadinIcons.TOOLS, menuItem -> getNavigator().navigateTo(EditAccountView.VIEW_NAME));
        MenuBar.Command toLogout = (MenuBar.Command) menuItem -> {
            getUI().getSession().close();
            getUI().getPage().setLocation("/logout");
        };
        logout = account.addItem("Kijelentkezés", VaadinIcons.SIGN_OUT, toLogout);

        menuBar.setSizeUndefined();
        baseLayout.addComponent(menuBar, 0, 1);
        baseLayout.setComponentAlignment(menuBar, Alignment.MIDDLE_CENTER);
    }

    private void addContentLayout(){
        viewContainer = new Panel();
        viewContainer.setHeight("100%");
        viewContainer.setWidth("100%");
        baseLayout.addComponent(viewContainer, 0, 2);
    }
    

    private void showUserInfo() {
        Window infoWindow = new Window("Statisztikám");
        Label name = new Label();
        Label type = new Label();
        type.setContentMode(ContentMode.PREFORMATTED);
        name.setValue(getUser().getName());
        type.setValue(getLabel(getUser().getType()));
        Label numReported = new Label();
        Label numCared = new Label();
        Label numReleased = new Label();
        Label numDied = new Label();
        Label rating = new Label();
        numReported.setValue("Bejelentett állatok: " + Math.round(userService.getReportedCount(user)));
        VerticalLayout info;
        if (user.getType().equals(UserType.RESCUER)) {
            numCared.setValue("Gondozott állatok: " + Math.round(userService.getCaredCount(user)));
            numReleased.setValue("Elengedett állatok: " + Math.round(userService.getReleasedCount(user)));
            numDied.setValue("Elpusztult állatok: " + Math.round(userService.getDeadCount(user)));
            rating.setValue("Túlélési arány: " + df.format(userService.rating(user)) + "%");
            info = new VerticalLayout(name, type, numReported, numCared, numReleased, numDied, rating);
        } else {
            info = new VerticalLayout(name, type, numReported);
        }
        infoWindow.setContent(info);
        infoWindow.center();
        infoWindow.setWidth(220, Unit.PIXELS);
        infoWindow.setModal(true);
        getUI().addWindow(infoWindow);
    }

    public User getUser(){
        return user;
    }

    private void restrictVisibility(){
        home.setVisible(true);
        reported.setVisible(true);
        hospitals.setVisible(true);
        rescuers.setVisible(true);
        information.setVisible(true);
        account.setVisible(true);
        login.setVisible(true);
        register.setVisible(true);
        User user = getUser();
        if(user != null) {
            if (user.getType().equals(UserType.ADMIN)) {
                menuBar.getItems().forEach(item -> item.setVisible(true));
                return;
            }
            boolean reporter = user.getType().equals(UserType.FINDER);
            boolean rescuer = user.getType().equals(UserType.RESCUER);
            report.setVisible(reporter || rescuer);
            heal.setVisible(rescuer);
            care.setVisible(rescuer);
            healing.setVisible(rescuer);
            edit.setVisible(true);
            login.setVisible(false);
            logout.setVisible(true);
            register.setVisible(false);
            admin.setVisible(false);
            statistics.setVisible(true);
            forgottenPassword.setVisible(false);
        } else {
            report.setVisible(false);
            heal.setVisible(false);
            edit.setVisible(false);
            logout.setVisible(false);
            admin.setVisible(false);
            statistics.setVisible(false);
            forgottenPassword.setVisible(true);
        }
    }


}

