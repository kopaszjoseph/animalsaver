package hu.elte.mullermate.animalsaver.view;

import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.shared.Position;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import hu.elte.mullermate.animalsaver.domain.User;
import hu.elte.mullermate.animalsaver.service.AnimalHospitalService;
import hu.elte.mullermate.animalsaver.service.AnimalService;
import hu.elte.mullermate.animalsaver.service.UserService;
import org.springframework.context.annotation.Scope;

import static hu.elte.mullermate.animalsaver.enums.UserType.getLabel;

/**
 * @author mullerm
 */
@Scope("vaadin-ui")
@SpringView(name = AdminView.VIEW_NAME)
public class AdminView extends VerticalLayout implements View {

    public static final String VIEW_NAME = "admin";

    private final UserService userService;
    private final AnimalService animalService;
    private final AnimalHospitalService animalHospitalService;

    private Grid<User> users;


    public AdminView(UserService userService, AnimalService animalService, AnimalHospitalService animalHospitalService) {
        this.userService = userService;
        this.animalService = animalService;
        this.animalHospitalService = animalHospitalService;

        addButtons();
        initUserGrid();
    }

    private void addButtons(){
        Button deleteAnimalsButton = new Button("Összes állat törlése");
        Button resetStatisticsButton = new Button("Felhasználói statisztikák törlése");
        Button deleteHospitalsButton = new Button("Összes kórház törlése");
        Button deleteUsersButton = new Button("Összes felhasználó törlése");
        Button addHospitalButton = new Button("Állatkórház felvétele");
        deleteAnimalsButton.setStyleName(ValoTheme.BUTTON_DANGER);
        deleteAnimalsButton.addClickListener(event -> {
            animalService.deleteAllAnimals();
            Notification.show("Összes állat törölve.",
                    Notification.Type.TRAY_NOTIFICATION).setPosition(Position.TOP_LEFT);
        });
        resetStatisticsButton.setStyleName(ValoTheme.BUTTON_DANGER);
        resetStatisticsButton.addClickListener(event -> {
            userService.resetAllUsersStatistics();
            updateUsers();
            Notification.show("Felhasználói statisztikák törölve.",
                    Notification.Type.TRAY_NOTIFICATION).setPosition(Position.TOP_LEFT);
        });
        deleteHospitalsButton.setStyleName(ValoTheme.BUTTON_DANGER);
        deleteHospitalsButton.addClickListener(event -> {
            animalHospitalService.deleteAll();
            Notification.show("Összes állatkórház törölve.",
                    Notification.Type.TRAY_NOTIFICATION).setPosition(Position.TOP_LEFT);
        });
        deleteUsersButton.setStyleName(ValoTheme.BUTTON_DANGER);
        deleteUsersButton.addClickListener(event -> {
            userService.adminDeleteAllUsers();
            updateUsers();
            Notification.show("Összes felhasználó törölve.",
                    Notification.Type.TRAY_NOTIFICATION).setPosition(Position.TOP_LEFT);
        });
        addHospitalButton.setStyleName(ValoTheme.BUTTON_FRIENDLY);
        addHospitalButton.addClickListener(event -> getUI().getNavigator().navigateTo(AddHospitalView.VIEW_NAME));
        HorizontalLayout buttons = new HorizontalLayout(deleteAnimalsButton, resetStatisticsButton, deleteHospitalsButton, deleteUsersButton, addHospitalButton);
        addComponent(buttons);
    }

    private void initUserGrid(){
        users = new Grid<>();
        users.setSizeFull();
        users.addColumn(User::getName).setCaption("Név");
        users.addColumn(User::getEmail).setCaption("Email");
        users.addComponentColumn(user -> new Label(getLabel(user.getType()))).setCaption("Típus");
        users.addColumn(User::getReportedCount).setCaption("Bejelentett");
        users.addColumn(User::getCaredCount).setCaption("Gondozott");
        users.addColumn(User::getReleasedCount).setCaption("Elengedett");
        users.addColumn(User::getDeadCount).setCaption("Eltemetett");
        users.addComponentColumn(this::buildDelButton).setCaption("Törlés");
        users.setItems(userService.findAll());
        addComponent(users);
    }

    private Button buildDelButton(User user){
        Button button = new Button(VaadinIcons.DEL);
        button.addStyleName(ValoTheme.BUTTON_ICON_ONLY);
        button.addStyleName(ValoTheme.BUTTON_DANGER);
        button.addClickListener(event -> confirmDelete(user));
        return button;
    }

    private void confirmDelete(User user){
        Window confirmWindow = new Window("Megerősítés");
        Button confirmButton = new Button("Igen");
        confirmButton.addClickListener(event -> {
            userService.adminDeleteAccount(user);
            getUI().getPage().reload();
            updateUsers();
            confirmWindow.close();
            Notification.show("Felhasználó sikeresen eltávolítva.",
                    Notification.Type.TRAY_NOTIFICATION).setPosition(Position.TOP_LEFT);
        });
        confirmButton.setStyleName(ValoTheme.BUTTON_DANGER);
        Button cancelButton = new Button("Mégsem");
        cancelButton.addClickListener(event -> confirmWindow.close());
        cancelButton.setStyleName(ValoTheme.BUTTON_FRIENDLY);
        HorizontalLayout buttons = new HorizontalLayout(cancelButton, confirmButton);
        Label confirmText = new Label("Biztosan törlöd a fiókot?");
        confirmText.setStyleName(ValoTheme.LABEL_H3);
        VerticalLayout confirmLayout = new VerticalLayout(confirmText, buttons);
        confirmLayout.setComponentAlignment(buttons, Alignment.BOTTOM_CENTER);
        confirmWindow.setContent(confirmLayout);
        confirmWindow.setModal(true);
        confirmWindow.center();
        confirmWindow.setSizeUndefined();
        getUI().addWindow(confirmWindow);
    }

    private void updateUsers(){
        users.getDataProvider().refreshAll();
        users.setItems(userService.findAll());
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent){
        getUI().getPage().setTitle("AnimalSaver - ADMIN");
        updateUsers();
    }

}
