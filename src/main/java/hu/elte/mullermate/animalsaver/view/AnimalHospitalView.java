package hu.elte.mullermate.animalsaver.view;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.shared.ui.ValueChangeMode;
import com.vaadin.shared.ui.grid.HeightMode;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.*;
import com.vaadin.ui.components.grid.HeaderRow;
import com.vaadin.ui.themes.ValoTheme;
import hu.elte.mullermate.animalsaver.domain.AnimalHospital;
import hu.elte.mullermate.animalsaver.enums.Province;
import hu.elte.mullermate.animalsaver.service.AnimalHospitalService;
import org.springframework.context.annotation.Scope;

import java.util.List;
import java.util.stream.Stream;

/**
 * @author mullerm
 */
@Scope("vaadin-ui")
@SpringView(name = AnimalHospitalView.VIEW_NAME)
public class AnimalHospitalView extends VerticalLayout implements View {

    public static final String VIEW_NAME = "animalhospitals";

    private AnimalHospitalService animalHospitalService;

    private Grid<AnimalHospital> hospitals;
    private final ComboBox<String> provinceFilter = new ComboBox<>();
    private final TextField nameFilter = new TextField();
    private final TextField cityFilter = new TextField();
    private HeaderRow filteringHeader;


    public AnimalHospitalView(AnimalHospitalService animalHospitalService){
        this.animalHospitalService = animalHospitalService;

        setHeightUndefined();
        setDefaultComponentAlignment(Alignment.TOP_CENTER);

        initGrid();
        initFilters();
    }

    private void initGrid(){
        hospitals = new Grid<>();
        hospitals.setHeightMode(HeightMode.UNDEFINED);
        hospitals.setWidth(100, Unit.PERCENTAGE);
        filteringHeader = hospitals.appendHeaderRow();
        hospitals.addColumn(AnimalHospital::getName).setCaption("Név").setId("name");
        hospitals.addColumn(AnimalHospital::getCity).setCaption("Város").setId("city");
        hospitals.addColumn(AnimalHospital::getAddress).setCaption("Cím").setId("address");
        hospitals.addColumn(AnimalHospital::getEmail).setCaption("E-mail").setId("email");
        hospitals.addColumn(AnimalHospital::getPhone).setCaption("Telefonszám");
        hospitals.addColumn(AnimalHospital::getWebsite).setCaption("Honlap");
        hospitals.setStyleGenerator(item -> "v-align-center");
        filteringHeader.setStyleName("v-align-center");
        addComponent(hospitals);
    }

    private void initFilters(){
        provinceFilter.setPlaceholder("Szűrés megyére...");
        provinceFilter.setItems(Province.labelValues());
        provinceFilter.setStyleName(ValoTheme.COMBOBOX_TINY);
        provinceFilter.addValueChangeListener(select -> searchForAllFilters());
        nameFilter.setPlaceholder("Keresés névre...");
        nameFilter.setValueChangeMode(ValueChangeMode.LAZY);
        nameFilter.setStyleName(ValoTheme.TEXTFIELD_TINY);
        nameFilter.addValueChangeListener(text ->  searchForAllFilters());
        cityFilter.setPlaceholder("Keresés városra...");
        cityFilter.setValueChangeMode(ValueChangeMode.LAZY);
        cityFilter.setStyleName(ValoTheme.TEXTFIELD_TINY);
        cityFilter.addValueChangeListener(text -> searchForAllFilters());
        Button clearFiltersButton = new Button("Összes szűrés törlése");
        clearFiltersButton.setStyleName(ValoTheme.BUTTON_TINY);
        clearFiltersButton.addClickListener(e -> clearFilters());
        filteringHeader.getCell("name").setComponent(nameFilter);
        filteringHeader.getCell("city").setComponent(cityFilter);
        filteringHeader.getCell("address").setComponent(provinceFilter);
        filteringHeader.getCell("email").setComponent(clearFiltersButton);
    }

    private void searchForAllFilters(){
        List<AnimalHospital> filtered = animalHospitalService.allFilters(provinceFilter.getSelectedItem().orElse(null),
                cityFilter.getValue(), nameFilter.getValue());
        if(filtered.size() > 0)
            hospitals.setItems(filtered);
        else {
            hospitals.setItems(Stream.empty());
            Notification.show("A keresés nem adott eredményt.",
                    "Próbáld kevesebb feltétellel!", Notification.Type.WARNING_MESSAGE).setDelayMsec(2222);
        }
    }

    private void clearFilters(){
        nameFilter.clear();
        cityFilter.clear();
        provinceFilter.clear();
    }

    private void updateGrid(){
        hospitals.getDataProvider().refreshAll();
        hospitals.setItems(animalHospitalService.showAll());
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent){
        getUI().getPage().setTitle("AnimalSaver - Állatkórházak");
        clearFilters();
        updateGrid();
    }
}
