package hu.elte.mullermate.animalsaver.view;

import com.vaadin.data.provider.Query;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.FileResource;
import com.vaadin.server.Page;
import com.vaadin.shared.Position;
import com.vaadin.shared.data.sort.SortDirection;
import com.vaadin.shared.ui.ValueChangeMode;
import com.vaadin.shared.ui.grid.HeightMode;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.*;
import com.vaadin.ui.components.grid.HeaderRow;
import com.vaadin.ui.themes.ValoTheme;
import hu.elte.mullermate.animalsaver.domain.Animal;
import hu.elte.mullermate.animalsaver.domain.User;
import hu.elte.mullermate.animalsaver.enums.AnimalCategory;
import hu.elte.mullermate.animalsaver.enums.AnimalState;
import hu.elte.mullermate.animalsaver.enums.Province;
import hu.elte.mullermate.animalsaver.service.AnimalService;
import hu.elte.mullermate.animalsaver.service.EmailService;
import org.springframework.context.annotation.Scope;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.util.List;
import java.util.stream.Stream;

import static hu.elte.mullermate.animalsaver.enums.AnimalCategory.getEnumValue;
import static hu.elte.mullermate.animalsaver.enums.AnimalCategory.getLabel;

/**
 * @author mullerm
 */
@Scope("vaadin-ui")
@SpringView(name = CareView.VIEW_NAME)
@Transactional
public class CareView extends VerticalLayout implements View {

    public static final String VIEW_NAME = "care";

    private final AnimalService animalService;
    private final EmailService emailService;

    private User rescuer;
    private Grid<Animal> animals;
    private final ComboBox<String> provinceFilter = new ComboBox<>();
    private final ComboBox<String> categoryFilter = new ComboBox<>();
    private final TextField cityFilter = new TextField();
    private Notification plsLogIn;


    public CareView(AnimalService animalService, EmailService emailService) {
        this.animalService = animalService;
        this.emailService = emailService;

        initUser();
        setSizeFull();
        setDefaultComponentAlignment(Alignment.TOP_CENTER);

        setupLoginNotification();
        initGrid();
        initFilter();
    }

    private void initUser(){
        rescuer = ((AdminUI) UI.getCurrent()).getUser();
    }

    private void initGrid() {
        animals = new Grid<>();
        animals.setHeightMode(HeightMode.UNDEFINED);
        animals.setWidth(100, Unit.PERCENTAGE);
        animals.addComponentColumn(animal -> {
            if (animal.getPicturePath() != null) {
                FileResource resource = new FileResource(new File(animal.getPicturePath()));
                Image image = new Image("Feltöltött kép", resource);
                Image full = new Image("Teljes méretű kép", resource);
                image.addClickListener(click -> openInFullSize(full));
                image.setWidth(100, Unit.PIXELS);
                image.setHeight(100, Unit.PIXELS);
                return image;
            } else
                return new Label("Nincs kép");
        }).setCaption("Kép").setId("picture");
        animals.addColumn(Animal::getFinderName).setCaption("Bejelentő").setId("reporter");
        animals.addComponentColumn(animal -> new Label(getLabel(animal.getCategory()))).setCaption("Kategória").setId("category");
        animals.addComponentColumn(animal -> {
            CheckBox damaged = new CheckBox();
            damaged.setReadOnly(true);
            if (animal.isDamaged()) {
                damaged.setValue(true);
                return damaged;
            } else {
                damaged.setValue(false);
                return damaged;
            }
        }).setCaption("Sérült").setId("damaged");
        animals.addColumn(Animal::getComment).setCaption("Megjegyzés");
        animals.addColumn(Animal::getCreatedTime).setCaption("Időpont").setId("date");
        animals.sort("date", SortDirection.DESCENDING);
        animals.addComponentColumn(this::buildEditButton).setCaption("Gondozás");
        animals.setBodyRowHeight(100);
        animals.setStyleGenerator(item -> "v-align-center");
        addComponent(animals);
    }

    private Button buildEditButton(Animal animal){
        Button button = new Button(VaadinIcons.DOCTOR_BRIEFCASE);
        button.addStyleName(ValoTheme.BUTTON_FRIENDLY);
        button.addClickListener(e -> contactReporter(animal));
        return button;
    }

    private void initFilter(){
        HeaderRow filterHeader = animals.appendHeaderRow();
        cityFilter.setPlaceholder("Szűrés városra...");
        cityFilter.addValueChangeListener(e -> searchForAllFilters());
        cityFilter.setStyleName(ValoTheme.TEXTFIELD_TINY);
        cityFilter.setWidthUndefined();
        cityFilter.setValueChangeMode(ValueChangeMode.LAZY);
        provinceFilter.setItems(Province.labelValues());
        provinceFilter.addValueChangeListener(e -> searchForAllFilters());
        provinceFilter.setStyleName(ValoTheme.COMBOBOX_TINY);
        provinceFilter.setPlaceholder("Szűrés megyére...");
        categoryFilter.setItems(AnimalCategory.labelValues());
        categoryFilter.addValueChangeListener(e -> searchForAllFilters());
        categoryFilter.setStyleName(ValoTheme.COMBOBOX_TINY);
        categoryFilter.setPlaceholder("Szűrés kategóriára...");
        Button clearFiltersButton = new Button("Összes szűrés törlése");
        clearFiltersButton.setStyleName(ValoTheme.BUTTON_TINY);
        clearFiltersButton.addClickListener(e -> clearFilters());
        filterHeader.getCell("picture").setComponent(provinceFilter);
        filterHeader.getCell("reporter").setComponent(cityFilter);
        filterHeader.getCell("category").setComponent(categoryFilter);
        filterHeader.getCell("damaged").setComponent(clearFiltersButton);
    }

    private void searchForAllFilters(){
        AnimalCategory category = getEnumValue(categoryFilter.getSelectedItem().orElse(null));
        List<Animal> filtered = animalService.allFilters(provinceFilter.getSelectedItem().orElse(null),
                cityFilter.getValue(), AnimalState.FOUND, category);
        if(filtered.size() > 0)
            animals.setItems(filtered);
        else {
            animals.setItems(Stream.empty());
            Notification.show("A szűrés nem adott eredményt.", Notification.Type.WARNING_MESSAGE);
        }
    }

    private void clearFilters(){
        cityFilter.clear();
        provinceFilter.clear();
        categoryFilter.clear();
    }

    private void openInFullSize(Image full){
        VerticalLayout windowContent = new VerticalLayout();
        windowContent.addComponent(full);
        Window fullSizeImage = new Window();
        fullSizeImage.setContent(windowContent);
        fullSizeImage.setSizeUndefined();
        fullSizeImage.center();
        fullSizeImage.setModal(true);
        getUI().addWindow(fullSizeImage);
    }

    private void careAnimal(Animal animal) {
        if (animal.getFinder() == null) {
            try {
                new Thread(() -> emailService.sendDeletedFinderCareMessage(rescuer, animal)).start();
                animalService.careAnimal(rescuer, animal);
                Notification successNotification = new Notification("Sikeresen gondozásba vetted az állatot!",
                        "Vedd fel a kapcsolatot a bejelentővel!", Notification.Type.TRAY_NOTIFICATION);
                successNotification.setPosition(Position.TOP_LEFT);
                successNotification.setDelayMsec(2000);
                successNotification.show(Page.getCurrent());
            } catch (IllegalArgumentException e) {
                Notification.show("Csak állatmentők gondozhatnak!", Notification.Type.ERROR_MESSAGE);
            }
        } else {
            try {
                new Thread(() -> emailService.sendCareMessage(rescuer, animal)).start();
                animalService.careAnimal(rescuer, animal);
                Notification successNotification = new Notification("Sikeresen gondozásba vetted az állatot!",
                        "Vedd fel a kapcsolatot a bejelentővel!", Notification.Type.TRAY_NOTIFICATION);
                successNotification.setPosition(Position.TOP_LEFT);
                successNotification.setDelayMsec(2000);
                successNotification.show(Page.getCurrent());
            } catch (IllegalArgumentException e){
                Notification.show("Csak állatmentők gondozhatnak!", Notification.Type.ERROR_MESSAGE);
            }
        }
    }

    private void contactReporter(Animal animal){
        Window careWindow = new Window("Kapcsolatfelvétel");
        User finder = animal.getFinder();
        Label name = new Label();
        Label email = new Label();
        Label phone = new Label();
        Label address = new Label();
        Button emailButton = new Button("e-mail");
        emailButton.setIcon(VaadinIcons.ENVELOPE);
        if(finder != null){
            name.setValue(finder.getName());
            email.setValue(finder.getEmail());
            phone.setValue(finder.getPhone());
            address.setValue(finder.getAddress());
            emailButton.setVisible(true);
            emailButton.addClickListener(e -> openEmailWindow(finder));
            emailButton.setStyleName(ValoTheme.BUTTON_FRIENDLY);
        } else {
            name.setValue("A felhasználó törölte magát.");
            email.setValue("Próbáld meg felhívni az alábbi számon:");
            phone.setValue(animal.getFinderPhone());
            address.setValue("");
            emailButton.setVisible(false);
        }
        Button careButton = new Button("Gondozásba veszem!");
        careButton.addClickListener(e -> {
            careAnimal(animal);
            updateGrid();
            careWindow.close();
            getUI().getNavigator().navigateTo(HealingView.VIEW_NAME);
            Notification successNotification = new Notification("Sikeresen gondozásba vetted az állatot!",
                    "Vedd fel a kapcsolatot a bejelentővel!", Notification.Type.TRAY_NOTIFICATION);
            successNotification.setPosition(Position.TOP_LEFT);
            successNotification.setDelayMsec(2000);
            successNotification.show(Page.getCurrent());
        });
        careButton.addStyleName(ValoTheme.BUTTON_PRIMARY);
        HorizontalLayout buttons = new HorizontalLayout(emailButton, careButton);
        VerticalLayout contact = new VerticalLayout(name, email, phone, address, buttons);
        contact.setSpacing(true);
        careWindow.setContent(contact);
        careWindow.setModal(true);
        careWindow.center();
        careWindow.setSizeUndefined();
        getUI().addWindow(careWindow);
    }

    private void openEmailWindow(User finder){
        Window emailWindow = new Window(" E-mail küldés");
        emailWindow.setIcon(VaadinIcons.PENCIL);
        emailWindow.setModal(true);
        TextArea emailContent = new TextArea();
        emailContent.clear();
        emailContent.setPlaceholder("Írj üzenetet!");
        emailContent.setSizeFull();
        Button send = new Button("Küldés");
        Button cancel = new Button("Mégsem");
        send.setStyleName(ValoTheme.BUTTON_PRIMARY);
        send.addClickListener(event -> {
            new Thread(() -> emailService.sendCustomMessage(rescuer, finder, emailContent.getValue())).start();
            emailWindow.close();
            Notification.show("Üzenet elküldve!",
                    Notification.Type.TRAY_NOTIFICATION).setPosition(Position.TOP_LEFT);
        });
        cancel.addClickListener(e -> emailWindow.close());
        HorizontalLayout buttons = new HorizontalLayout(cancel, send);
        Label emailAddress = new Label();
        emailAddress.setValue(finder.getEmail());
        emailAddress.setStyleName(ValoTheme.LABEL_BOLD);
        Label header = new Label("Vedd fel a kapcsolatot a felhasználóval!");
        VerticalLayout windowLayout = new VerticalLayout(header, emailAddress, emailContent, buttons);
        windowLayout.setComponentAlignment(buttons, Alignment.BOTTOM_RIGHT);
        emailWindow.setContent(windowLayout);
        emailWindow.center();
        emailWindow.setHeight(350, Unit.PIXELS);
        emailWindow.setWidth(700, Unit.PIXELS);
        getUI().addWindow(emailWindow);
    }

    private void setupLoginNotification(){
        plsLogIn = new Notification ("Jelentkezz be a folytatáshoz!", Notification.Type.ERROR_MESSAGE);
        plsLogIn.addCloseListener(event -> getUI().getPage().setLocation("/register"));
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent){
        getUI().getPage().setTitle("AnimalSaver - Gondozás");
        if(rescuer == null){
            plsLogIn.show(Page.getCurrent());
        }
        updateGrid();
        if(animals.getDataProvider().size(new Query<>()) == 0)
            Notification.show("Jelenleg nincs gondozásra váró állat.", Notification.Type.WARNING_MESSAGE);
    }

    private void updateGrid(){
        animals.getDataProvider().refreshAll();
        animals.setItems(animalService.showReportedAnimals());
    }
}
