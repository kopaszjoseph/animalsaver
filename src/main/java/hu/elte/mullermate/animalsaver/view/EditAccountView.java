package hu.elte.mullermate.animalsaver.view;

import com.vaadin.data.Binder;
import com.vaadin.data.Validator;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.Page;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import hu.elte.mullermate.animalsaver.domain.User;
import hu.elte.mullermate.animalsaver.enums.Province;
import hu.elte.mullermate.animalsaver.service.UserService;
import org.springframework.context.annotation.Scope;

import java.util.Objects;

/**
 * @author mullerm
 */
@Scope("vaadin-ui")
@SpringView(name = EditAccountView.VIEW_NAME)
public class EditAccountView extends FormLayout implements View {

    public static final String VIEW_NAME = "editAccount";

    private UserService userService;

    private Binder<User> binder;
    private User current;
    private User update;
    private TextField name = new TextField("Név:");
    private TextField phone = new TextField("Telefonszám:");
    private ComboBox<String> province = new ComboBox<>("Megye:");
    private TextField address = new TextField("Lakcím");
    private Notification plsLogIn = new Notification ("", Notification.Type.ERROR_MESSAGE);


    public EditAccountView(UserService userService){
        this.userService = userService;

        initUser();
        setMargin(true);
        initHeader();
        initBinder();
    }

    private void initUser(){
        current = ((AdminUI) UI.getCurrent()).getUser();
    }

    private void initHeader(){
        Label modify = new Label("Személyes adatok módosítása");
        modify.setStyleName(ValoTheme.LABEL_H3);
        addComponent(modify);
    }

    private void initBinder(){
        binder = new Binder<>();

        name.focus();
        name.setWidth(250, Unit.PIXELS);
        binder.forField(name).withValidator(name -> name.length() >= 3 || name.isEmpty(),
                "Minimum 3 karakter!").bind(User::getName, User::setName);
        name.setIcon(VaadinIcons.TEXT_LABEL);

        phone.setWidth(150, Unit.PIXELS);
        binder.forField(phone).withValidator(phone -> phone.length() == 12 || phone.isEmpty(),
                "Formátum: +36xxxxxxxxx").bind(User::getPhone, User::setPhone);
        phone.setIcon(VaadinIcons.PHONE);

        province.setWidth(250, Unit.PIXELS);
        province.setItems(Province.labelValues());
        binder.forField(province).bind(User::getProvince, User::setProvince);
        province.setIcon(VaadinIcons.MAP_MARKER);

        address.setWidth(250, Unit.PIXELS);
        binder.bind(address, User::getAddress, User::setAddress);
        address.setIcon(VaadinIcons.HOME);

        Button saveButton = new Button("Mentés");
        saveButton.setEnabled(false);

        binder.addStatusChangeListener(event -> saveButton.setEnabled(binder.isValid()));

        addComponents(name, province, address, phone);

        Button passwordButton = new Button("Jelszó módosítás");
        passwordButton.addClickListener(event -> changePassword());
        saveButton.addStyleName(ValoTheme.BUTTON_PRIMARY);
        saveButton.addClickListener(event -> {
            update = binder.getBean();
            save();
        });

        Button deleteButton = new Button("Fiók törlése");
        deleteButton.addClickListener(event -> delete());
        HorizontalLayout buttons = new HorizontalLayout(deleteButton, passwordButton, saveButton);
        addComponent(buttons);
    }


    private void save(){
        try{
            checkData();
            userService.updateUser(current, update);
            setupReLoginNotification();
            plsLogIn.show(Page.getCurrent());
        } catch (IllegalArgumentException e){
            Notification.show("Hiba a módosítás közben.", Notification.Type.ERROR_MESSAGE);
            e.printStackTrace();
        }
    }

    private void delete(){
        Window confirm = new Window(" Biztosan törlöd a fiókot?");
        confirm.setIcon(VaadinIcons.WARNING);
        PasswordField confirmPassword = new PasswordField("Írd be a jelszavad!");
        confirmPassword.focus();
        confirmPassword.setWidth(150, Unit.PIXELS);
        Button yes = new Button("Igen");
        yes.setStyleName(ValoTheme.BUTTON_DANGER);
        yes.addClickListener(event ->{
            try {
                userService.deleteAccount(current, confirmPassword.getValue());
                getUI().getSession().close();
                getUI().getPage().setLocation("/logout");
            }catch (IllegalArgumentException e){
                Notification.show("Rossz jelszó!", "Próbáld újra!", Notification.Type.ERROR_MESSAGE);
            }
        });
        Button no = new Button("Mégsem");
        no.addClickListener(event -> confirm.close());
        HorizontalLayout buttons = new HorizontalLayout(no, yes);
        Label warn = new Label("Minden adatod el fog veszni.\nÚjra kell majd regisztrálnod.", ContentMode.PREFORMATTED);
        VerticalLayout dialog = new VerticalLayout(warn, confirmPassword, buttons);
        dialog.setComponentAlignment(warn, Alignment.TOP_CENTER);
        dialog.setComponentAlignment(confirmPassword, Alignment.MIDDLE_CENTER);
        dialog.setComponentAlignment(buttons, Alignment.BOTTOM_CENTER);

        confirm.setContent(dialog);
        confirm.setWidth(350, Unit.PIXELS);
        confirm.setHeight(300, Unit.PIXELS);
        confirm.setModal(true);
        confirm.center();
        getUI().addWindow(confirm);
    }

    private void changePassword(){
        Window change = new Window(" Jelszó módosítás");
        change.setIcon(VaadinIcons.LOCK);
        Binder<User> pwdBinder = new Binder<>();
        pwdBinder.setBean(new User());

        PasswordField password = new PasswordField("Jelszó:");
        password.setWidth(150, Unit.PIXELS);
        password.setIcon(VaadinIcons.PASSWORD);
        pwdBinder.forField(password).bind(User::getPassword, User::setPassword);

        Label validationStatus = new Label();
        pwdBinder.setStatusLabel(validationStatus);

        PasswordField checkPassword = new PasswordField("Jelszó mégegyszer:");
        checkPassword.setWidth(150, Unit.PIXELS);
        checkPassword.setIcon(VaadinIcons.PASSWORD);
        pwdBinder.forField(checkPassword).asRequired("Kötelező a megerősítés!").bind(User::getPassword, (user, pwd) -> {});

        Button modify = new Button("Módosítás");
        Button no = new Button("Mégsem");
        HorizontalLayout buttons = new HorizontalLayout(no, modify);
        modify.setEnabled(false);
        modify.setStyleName(ValoTheme.BUTTON_PRIMARY);

        no.addClickListener(event -> change.close());

        pwdBinder.withValidator(Validator.from(user -> {
            if(password.isEmpty() || checkPassword.isEmpty()){
                return true;
            } else {
                return Objects.equals(password.getValue(), checkPassword.getValue()) && password.getValue().length() >= 6;
            }
        }, "A jelszavaknak egyezni kell, min. 6 karakter!"));
        pwdBinder.addStatusChangeListener( event -> modify.setEnabled(pwdBinder.isValid()));

        modify.addClickListener(event -> {
            userService.updatePassword(current, pwdBinder.getBean());
            change.close();
            setupReLoginNotification();
            plsLogIn.show(Page.getCurrent());
        });

        VerticalLayout cont = new VerticalLayout(password, checkPassword, validationStatus, buttons);
        cont.setComponentAlignment(password, Alignment.TOP_CENTER);
        cont.setComponentAlignment(checkPassword, Alignment.TOP_CENTER);
        cont.setComponentAlignment(validationStatus, Alignment.MIDDLE_CENTER);
        cont.setComponentAlignment(buttons, Alignment.BOTTOM_CENTER);
        change.setContent(cont);
        change.center();
        change.setWidth(350, Unit.PIXELS);
        change.setHeight(300, Unit.PIXELS);
        change.setModal(true);
        getUI().getUI().addWindow(change);
    }

    private void setupReLoginNotification(){
        plsLogIn.setCaption("Sikeres mentés");
        plsLogIn.setDescription("Jelentkezz be újra!");
        plsLogIn.addCloseListener(click -> getUI().getPage().setLocation("/login"));
    }

    private void setupLoginNotification(){
        plsLogIn.setCaption("Jelentkezz be a folytatáshoz!");
        plsLogIn.setDescription("");
        plsLogIn.addCloseListener(event -> getUI().getPage().setLocation("/register"));
    }

    private void setPlaceHolders(){
        name.setPlaceholder(current.getName());
        phone.setPlaceholder(current.getPhone());
        province.setPlaceholder(current.getProvince());
        address.setPlaceholder(current.getAddress());
    }

    private void checkData(){
        if(name.isEmpty())
            name.setValue(current.getName());
        if(phone.isEmpty())
            phone.setValue(current.getPhone());
        if(province.isEmpty())
            province.setValue(current.getProvince());
        if(address.isEmpty())
            address.setValue(current.getAddress());
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent){
        getUI().getPage().setTitle("AnimalSaver - Fiók szerkesztése");
        if(current != null){
            setPlaceHolders();
            binder.setBean(new User());
        } else {
            setupLoginNotification();
            plsLogIn.show(Page.getCurrent());
        }
    }



}
