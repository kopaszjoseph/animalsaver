package hu.elte.mullermate.animalsaver.view;

import com.vaadin.data.Binder;
import com.vaadin.data.Validator;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.Page;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import hu.elte.mullermate.animalsaver.domain.User;
import hu.elte.mullermate.animalsaver.enums.UserType;
import hu.elte.mullermate.animalsaver.service.UserService;
import org.springframework.context.annotation.Scope;

import java.util.Objects;

/**
 * @author mullerm
 */
@Scope("vaadin-ui")
@SpringView(name = EditView.VIEW_NAME)
public class EditView extends FormLayout implements View {

    public static final String VIEW_NAME = "edit";

    private UserService userService;

    private Binder<User> binder = new Binder<>(User.class);
    private User user;
    private User update;


    private Button saveButton = new Button("Mentés");

    public EditView(UserService userService){
        this.userService = userService;
        user = ((AdminUI) UI.getCurrent()).getUser();
        setMargin(true);

        binder.bindInstanceFields(this);
        RadioButtonGroup<UserType> type = new RadioButtonGroup<>("Típus:");
        type.setItems(UserType.FINDER, UserType.RESCUER);

        TextField name = new TextField("Név:");
        TextField phone = new TextField("Telefonszám:");
        TextField address = new TextField("Lakcím:");
        addComponents(name, phone, address, type);

        Button passwordButton = new Button("Jelszó módosítás");
        passwordButton.addClickListener(event -> changePassword());
        saveButton.addStyleName(ValoTheme.BUTTON_PRIMARY);
        saveButton.setEnabled(false);


        Label validationStatus = new Label();
        binder.setStatusLabel(validationStatus);
        addComponent(validationStatus);
        binder.addStatusChangeListener(event -> saveButton.setEnabled(binder.isValid()));

        saveButton.addClickListener(event -> {
            update = binder.getBean();
            save();
        });
        Button deleteButton = new Button("Törlés");
        deleteButton.addClickListener(event -> delete());
        HorizontalLayout buttons = new HorizontalLayout(saveButton, passwordButton, deleteButton);
        addComponent(buttons);
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent){
        getUI().getPage().setTitle("AnimalSaver - Fiók szerkesztése");
        if(user != null){
            binder.setBean(user);
            binder.readBean(user);
        } else {
            Notification plsLogIn = new Notification ("Jelentkezz be a folytatáshoz!", Notification.Type.ERROR_MESSAGE);
            plsLogIn.addCloseListener(event -> getUI().getPage().setLocation("/register"));
            plsLogIn.show(Page.getCurrent());
        }
    }

    private void save(){
        try{
            userService.updateUser(user, update);
            getUI().getPage().setLocation("/home");
        } catch (IllegalArgumentException e){
            System.err.println("An error occurred during save.");
            e.printStackTrace();
        }
    }

    private void delete(){
        final Window confirm = new Window("Biztosan törlöd a fiókot?");
        Button yes = new Button("Igen");
        Button no = new Button("Mégsem");
        Label warn = new Label("Minden adatod el fog veszni.\nÚjra kell majd regisztrálnod.", ContentMode.PREFORMATTED);
        PasswordField confirmPassword = new PasswordField("Írd be a jelszavad!");
        confirmPassword.focus();
        yes.addClickListener(event ->{
            try {
                userService.deleteAccount(user, confirmPassword.getValue());
                getUI().getSession().close();
                getUI().getPage().setLocation("/logout");
            }catch (IllegalArgumentException e){
                Notification.show("Próbáld újra!", Notification.Type.WARNING_MESSAGE).show(Page.getCurrent());
                e.printStackTrace();
            }
        });
        no.setStyleName(ValoTheme.BUTTON_FRIENDLY);
        no.addClickListener(event -> confirm.close());
        final HorizontalLayout buttons = new HorizontalLayout(yes, no);
        final VerticalLayout dialog = new VerticalLayout(warn, confirmPassword, buttons);
        confirm.setContent(dialog);
        confirm.setWidth(300, Unit.PIXELS);
        confirm.center();

        getUI().addWindow(confirm);
    }

    private void changePassword(){
        final Window change = new Window("Jelszó módosítás");
        PasswordField password = new PasswordField("Jelszó:");
        PasswordField checkPassword = new PasswordField("Jelszó mégegyszer:");
        binder.forField(password).bind(User::getPassword, User::setPassword);

        Label validationStatus = new Label();
        binder.setStatusLabel(validationStatus);

        Button modify = new Button("Módosítás");
        modify.setEnabled(false);
        modify.setStyleName(ValoTheme.BUTTON_PRIMARY);
        Button no = new Button("Mégsem");
        no.addClickListener(event -> change.close());
        final HorizontalLayout buttons = new HorizontalLayout(modify, no);

        binder.withValidator(Validator.from(event -> {
            if(password.isEmpty() || checkPassword.isEmpty()){
                return true;
            } else {
                return Objects.equals(password.getValue(), checkPassword.getValue());
            }
        }, "A két jelszónak meg kell egyeznie!"));
        binder.addStatusChangeListener( asd -> modify.setEnabled(binder.isValid()));

        modify.addClickListener(event -> {
                binder.getBean().setPassword(password.getValue());
                change.close();
            });

        final VerticalLayout cont = new VerticalLayout(password, checkPassword, validationStatus, buttons);
        change.setContent(cont);
        change.center();
        change.setSizeUndefined();
        change.setModal(true);
        getUI().getUI().addWindow(change);
    }
}
