package hu.elte.mullermate.animalsaver.view;

import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import hu.elte.mullermate.animalsaver.domain.User;
import hu.elte.mullermate.animalsaver.service.EmailService;
import hu.elte.mullermate.animalsaver.service.UserService;
import org.springframework.context.annotation.Scope;

/**
 * @author mullerm
 */
@Scope("vaadin-ui")
@SpringView(name = ForgottenPasswordView.VIEW_NAME)
public class ForgottenPasswordView extends FormLayout implements View {

    public static final String VIEW_NAME = "forgotten_password";

    private final EmailService emailService;
    private final UserService userService;

    public ForgottenPasswordView(EmailService emailService, UserService userService) {
        this.emailService = emailService;
        this.userService = userService;

        this.setMargin(true);
        initHeader();
        setupContent();
    }

    private void initHeader() {
        Label forgotten = new Label("Elfelejtett jelszó");
        forgotten.setStyleName(ValoTheme.LABEL_H3);
        addComponent(forgotten);
    }

    private void setupContent() {
        Label question = new Label("Írd be az e-mail címed és segítünk Neked a jelszavad helyreállításában.");
        TextField emailField = new TextField("e-mail:");
        emailField.focus();
        emailField.setWidth(250, Unit.PIXELS);
        emailField.setIcon(VaadinIcons.AT);
        emailField.setRequiredIndicatorVisible(true);

        Button sendButton = new Button("Küldés");
        sendButton.setStyleName(ValoTheme.BUTTON_PRIMARY);
        sendButton.addClickListener(event -> {
            try {
                User user = userService.findByEmail(emailField.getValue());
                user.setPassword("AnimalSaverApplication2020");
                userService.updatePassword(user, user);
                new Thread(() -> emailService.sendForgottenPasswordMessage(user)).start();
                Notification.show("Ellenőrizd az e-mail fiókod!", Notification.Type.HUMANIZED_MESSAGE);
            } catch (IllegalArgumentException exception) {
                Notification.show("Nem található regisztráció ezzel az e-mail címmel!", Notification.Type.ERROR_MESSAGE);
            }
        });
        addComponents(question, emailField, sendButton);
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {
        getUI().getPage().setTitle("AnimalSaver - Elfelejtett jelszó");
    }

}
