package hu.elte.mullermate.animalsaver.view;

import com.vaadin.data.provider.Query;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.FileResource;
import com.vaadin.server.Page;
import com.vaadin.shared.Position;
import com.vaadin.shared.ui.grid.HeightMode;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import hu.elte.mullermate.animalsaver.domain.Animal;
import hu.elte.mullermate.animalsaver.domain.User;
import hu.elte.mullermate.animalsaver.service.AnimalService;
import org.springframework.context.annotation.Scope;

import java.io.File;

import static hu.elte.mullermate.animalsaver.enums.AnimalCategory.getLabel;

/**
 * @author mullerm
 */
@Scope("vaadin-ui")
@SpringView(name = HealingView.VIEW_NAME)
public class HealingView extends VerticalLayout implements View {

    public static final String VIEW_NAME = "healing";

    private final AnimalService animalService;

    private User rescuer;
    private TabSheet careTab;
    private Grid<Animal> healingAnimals;
    private Grid<Animal> healedAnimals;
    private Notification plsLogIn;


    public HealingView(AnimalService animalService) {
        this.animalService = animalService;

        setupLoginNotification();
        initUser();
        initGrids();
    }

    private void initUser(){
        rescuer = ((AdminUI) UI.getCurrent()).getUser();
        if(rescuer == null){
            plsLogIn.show(Page.getCurrent());
        }
    }

    private void initGrids(){
        careTab = new TabSheet();
        careTab.addStyleName(ValoTheme.TABSHEET_FRAMED);
        careTab.addStyleName(ValoTheme.TABSHEET_PADDED_TABBAR);
        healingAnimals = new Grid<>();
        healingAnimals.setHeightMode(HeightMode.UNDEFINED);
        healingAnimals.setWidth(100, Unit.PERCENTAGE);
        healedAnimals = new Grid<>();
        healedAnimals.setHeightMode(HeightMode.UNDEFINED);
        healedAnimals.setWidth(100, Unit.PERCENTAGE);

        healingAnimals.addComponentColumn(animal -> {
            if(animal.getPicturePath() != null){
                FileResource resource = new FileResource(new File(animal.getPicturePath()));
                Image image = new Image("Feltöltött kép", resource);
                Image full = new Image("Teljes méretű kép", resource);
                image.addClickListener(click -> openInFullSize(full));
                image.setWidth(100, Unit.PIXELS);
                image.setHeight(100, Unit.PIXELS);
                return image;
            } else
                return new Label("Nincs kép");
        }).setCaption("Kép").setId("picture");
        healingAnimals.addComponentColumn(animal -> new Label(getLabel(animal.getCategory()))).setCaption("Kategória");
        healingAnimals.addComponentColumn(animal -> {
            CheckBox damaged = new CheckBox();
            damaged.setValue(animal.isDamaged());
            damaged.addValueChangeListener(click -> {
                animalService.changeDamagedStatus(animal);
                String status = click.getValue() ? "sérült" : "nem sérült";
                Notification.show("Sérült státusz megváltoztatva: " + status,
                        Notification.Type.TRAY_NOTIFICATION).setPosition(Position.TOP_LEFT);
            });
            return damaged;
        }).setCaption("Sérült");
        healingAnimals.addColumn(Animal::getComment).setCaption("Megjegyzés");
        healingAnimals.addComponentColumn(this::buildEditButton).setCaption("Szerkesztés");
        healingAnimals.setBodyRowHeight(100);
        healingAnimals.setStyleGenerator(item -> "v-align-center");

        careTab.addTab(healingAnimals, "Gondozás alatt");

        healedAnimals.addComponentColumn(animal -> {
            if(animal.getPicturePath() != null){
                FileResource resource = new FileResource(new File(animal.getPicturePath()));
                Image image = new Image("Feltöltött kép", resource);
                Image full = new Image("Teljes méretű kép", resource);
                image.addClickListener(click -> openInFullSize(full));
                image.setWidth(100, Unit.PIXELS);
                image.setHeight(100, Unit.PIXELS);
                return image;
            } else {
                return new Label("Nincs kép");
            }
        }).setCaption("Kép").setId("picture");
        healedAnimals.addComponentColumn(animal -> new Label(getLabel(animal.getCategory()))).setCaption("Kategória");
        healedAnimals.addComponentColumn(animal -> {
            CheckBox damaged = new CheckBox();
            damaged.setReadOnly(true);
            if (animal.isDamaged()) {
                damaged.setValue(true);
                return damaged;
            } else {
                damaged.setValue(false);
                return damaged;
            }
        }).setCaption("Sérült");
        healedAnimals.addComponentColumn(animal -> {
            switch(animal.getState()){
                case FOUND:
                    return new Label("ÚJ");
                case CARE:
                    return new Label("GONDOZVA");
                case RELEASED:
                    return new Label("ELENGEDVE");
                case DEAD:
                    return new Label("ELPUSZTULT");
            }
            return new Label("ISMERETLEN");
        }).setCaption("Státusz");
        healedAnimals.setBodyRowHeight(100);
        healedAnimals.setStyleGenerator(item -> "v-align-center");

        careTab.addTab(healedAnimals, "Eddig gondozva");

        addComponent(careTab);
    }

    private void openInFullSize(Image full){
        VerticalLayout windowContent = new VerticalLayout();
        windowContent.addComponent(full);
        Window fullSizeImage = new Window();
        fullSizeImage.setIcon(VaadinIcons.PICTURE);
        fullSizeImage.setContent(windowContent);
        fullSizeImage.setSizeUndefined();
        fullSizeImage.center();
        fullSizeImage.setModal(true);
        getUI().addWindow(fullSizeImage);
    }

    private void setupLoginNotification(){
        plsLogIn = new Notification ("Jelentkezz be a folytatáshoz!", Notification.Type.ERROR_MESSAGE);
        plsLogIn.addCloseListener(event -> getUI().getPage().setLocation("/register"));
    }


    private Button buildEditButton(Animal animal){
        Button button = new Button(VaadinIcons.STETHOSCOPE);
        button.addStyleName(ValoTheme.BUTTON_FRIENDLY);
        button.addClickListener(e -> manageAnimal(animal));
        return button;
    }

    private void manageAnimal(Animal animal){
        Window manageWindow = new Window(" Státusz módosítás");
        manageWindow.setIcon(VaadinIcons.DOCTOR);
        Label text = new Label("Válassz!");
        Button releaseButton = new Button("Elengedtem");
        releaseButton.addClickListener(event -> {
            try{
                animalService.releaseAnimal(rescuer, animal);
                Notification.show("Elengedtél egy állatot.",
                        Notification.Type.TRAY_NOTIFICATION).setPosition(Position.TOP_LEFT);
            } catch (IllegalArgumentException e){
                System.err.println("WRONG USERTYPE OR USER IS NOT FOUND");
            } catch (NullPointerException e){
                System.err.println("USER/ANIMAL CAN'T BE NULL");
            }
            updateGrid();
            manageWindow.close();
        });
        releaseButton.addStyleName(ValoTheme.BUTTON_FRIENDLY);
        Button buryButton = new Button("Elpusztult");
        buryButton.addClickListener(event -> {
            try{
                animalService.buryAnimal(rescuer, animal);
                Notification.show("Eltemettél egy állatot.",
                        Notification.Type.TRAY_NOTIFICATION).setPosition(Position.TOP_LEFT);
            } catch(IllegalArgumentException e){
                System.err.println("WRONG USERTYPE OR USER IS NOT FOUND");
            } catch(NullPointerException e){
                System.err.println("USER/ANIMAL CAN'T BE NULL");
            }
            updateGrid();
            manageWindow.close();
        });
        HorizontalLayout buttons = new HorizontalLayout(buryButton, releaseButton);
        VerticalLayout window = new VerticalLayout(text, buttons);
        manageWindow.setModal(true);
        manageWindow.setContent(window);
        manageWindow.setSizeUndefined();
        manageWindow.center();
        getUI().addWindow(manageWindow);
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent){
        getUI().getPage().setTitle("AnimalSaver - Gondozott állatok");
        if (rescuer != null)
            updateGrid();
        if (healingAnimals.getDataProvider().size(new Query<>()) == 0)
            Notification.show("Jelenleg nincs gondozásban lévő állatod!", Notification.Type.WARNING_MESSAGE);
        else if (healedAnimals.getDataProvider().size(new Query<>()) == 0)
            Notification.show("Még nem gondoztál egy állatot sem!", Notification.Type.WARNING_MESSAGE);
    }

    private void updateGrid(){
        healingAnimals.getDataProvider().refreshAll();
        healingAnimals.setItems(animalService.healingAnimalsByUser(rescuer));
        healedAnimals.getDataProvider().refreshAll();
        healedAnimals.setItems(animalService.healedAnimalsByUser(rescuer));
    }
}
