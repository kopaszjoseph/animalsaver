package hu.elte.mullermate.animalsaver.view;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import org.springframework.context.annotation.Scope;

/**
 * @author mullerm
 */
@Scope("vaadin-ui")
@SpringView(name = HomeView.VIEW_NAME)
public class HomeView extends VerticalLayout implements View {

    public static final String VIEW_NAME = "home";

    private static boolean asked = false;


    public HomeView(){
        setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);
        Label greet = new Label("\n\nÜdv!\n" + "Kérlek olvasd el az információt az oldal használatáról!");
        greet.setSizeUndefined();
        addComponent(greet);
        setMargin(true);
    }

    private void askForLogin(){
        Window askWindow = new Window("Van már fiókod?");
        Button login = new Button("Belépek");
        login.addClickListener(event -> getUI().getPage().setLocation("/login"));
        login.setStyleName(ValoTheme.BUTTON_PRIMARY);
        Button register = new Button("Regisztrálok");
        register.addClickListener(event -> {
            getUI().getPage().setLocation("/register");
            askWindow.close();
        });
        register.setStyleName(ValoTheme.BUTTON_FRIENDLY);
        Button guest = new Button("Csak körbenézek");
        guest.addClickListener(event -> {
            asked = true;
            askWindow.close();
        });
        HorizontalLayout buttons = new HorizontalLayout(login, register);
        Label question = new Label("Ha már regisztráltál, jelentkezz be!");
        VerticalLayout askLayout = new VerticalLayout(question, buttons, guest);
        askLayout.setComponentAlignment(guest, Alignment.BOTTOM_CENTER);
        askWindow.setContent(askLayout);
        askWindow.center();
        askWindow.setModal(true);
        askWindow.setSizeUndefined();
        getUI().addWindow(askWindow);
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent){
        getUI().getPage().setTitle("AnimalSaver - Főoldal");
        if(!asked && ((AdminUI) UI.getCurrent()).getUser() == null) {
            askForLogin();
        }
    }

}
