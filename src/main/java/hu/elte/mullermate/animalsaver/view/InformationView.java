package hu.elte.mullermate.animalsaver.view;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import org.springframework.context.annotation.Scope;

/**
 * @author mullerm
 */
@Scope("vaadin-ui")
@SpringView(name = InformationView.VIEW_NAME)
public class InformationView extends VerticalLayout implements View {

    public static final String VIEW_NAME = "information";

    private TabSheet info = new TabSheet();

    private final Label label2 = new Label("<p>Az <b>ízeltlábúak</b> azonosításához szükséges leírás...", ContentMode.HTML);

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {
        getUI().getPage().setTitle("AnimalSaver - Információ");
        Notification.show("Kérjük, a bejelentés előtt olvasd el a rövid leírásokat!",
                "A kategóriaválasztáshoz kelleni fog.", Notification.Type.WARNING_MESSAGE).setDelayMsec(2000);
    }

    private final Label label1 = new Label("<p>Az oldal teljeskörű használatához regisztráció szükséges!</p>" +
            "<p><b>A látogatónak lehetősége van:</b></p>" +
            "<ul> " +
            "<li>Bejelentett állatok megtekintésére</li> " +
            "<li>Állatkórházak megtekintésére</li>" +
            "<li>Információ olvasására</li>" +
            "<li>Regisztrációra</li>" +
            "</ul>" +
            "<p><b>A bejelentőnek lehetősége van:</b></p>" +
            "<ul>" +
            "<li>Bejelentkezésre</li>" +
            "<li>Állat bejelentésére</li>" +
            "<li>Állatkórházak megtekintésére</li>" +
            "<li>Állatvédőkkel való kapcsolatfelvételre</li>" +
            "<li>Információ olvasására</li>" +
            "<li>Fiók módosítására</li>" +
            "</ul>" +
            "<p><b>Az állatvédőnek lehetősége van:</b></p>" +
            "<ul>" +
            "<li>Bejelentkezésre</li>" +
            "<li>Állat bejelentésére</li>" +
            "<li>Állat gondozására</li>" +
            "<li>Állatkórházak megtekintésére</li>" +
            "<li>Állatvédőkkel való kapcsolatfelvételre</li>" +
            "<li>Információ olvasására</li>" +
            "<li>Fiók módosítására</li>" +
            "</ul>"
            , ContentMode.HTML);
    private final Label label3 = new Label("<p>Az <b>halak</b> azonosításához szükséges leírás...", ContentMode.HTML);
    private final Label label4 = new Label("<p>Az <b>kétéltűek</b> azonosításához szükséges leírás...", ContentMode.HTML);
    private final Label label5 = new Label("<p>Az <b>hüllők</b> azonosításához szükséges leírás...", ContentMode.HTML);
    private final Label label6 = new Label("<p>Az <b>madarak</b> azonosításához szükséges leírás...", ContentMode.HTML);
    private final Label label7 = new Label("<p>Az <b>emlősök</b> azonosításához szükséges leírás...", ContentMode.HTML);

    public InformationView() {
        info.setHeight(100, Unit.PERCENTAGE);
        info.addStyleName(ValoTheme.TABSHEET_FRAMED);
        info.addStyleName(ValoTheme.TABSHEET_PADDED_TABBAR);

        HorizontalLayout tab1 = new HorizontalLayout(label1);
        tab1.setSizeFull();
        tab1.setMargin(true);

        info.addTab(tab1, "Információ");

        HorizontalLayout tab2 = new HorizontalLayout();
        tab2.setSizeFull();
        tab2.setMargin(true);
        tab2.addComponents(label2);

        info.addTab(tab2, "Ízeltlábúak");

        HorizontalLayout tab3 = new HorizontalLayout();
        tab3.setSizeFull();
        tab3.setMargin(true);
        tab3.addComponents(label3);

        info.addTab(tab3, "Halak");

        HorizontalLayout tab4 = new HorizontalLayout();
        tab4.setSizeFull();
        tab4.setMargin(true);
        tab4.addComponents(label4);

        info.addTab(tab4, "Kétéltűek");

        HorizontalLayout tab5 = new HorizontalLayout();
        tab5.setSizeFull();
        tab5.setMargin(true);
        tab5.addComponents(label5);

        info.addTab(tab5, "Hüllők");

        HorizontalLayout tab6 = new HorizontalLayout();
        tab6.setSizeFull();
        tab6.setMargin(true);
        tab6.addComponents(label6);

        info.addTab(tab6, "Madarak");

        HorizontalLayout tab7 = new HorizontalLayout();
        tab7.setSizeFull();
        tab7.setMargin(true);
        tab7.addComponents(label7);

        info.addTab(tab7, "Emlősök");

        addComponent(info);
    }
}
