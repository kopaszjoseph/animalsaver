package hu.elte.mullermate.animalsaver.view;

import com.vaadin.data.Binder;
import com.vaadin.data.Validator;
import com.vaadin.data.validator.EmailValidator;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.shared.Position;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import hu.elte.mullermate.animalsaver.domain.User;
import hu.elte.mullermate.animalsaver.enums.Province;
import hu.elte.mullermate.animalsaver.enums.UserType;
import hu.elte.mullermate.animalsaver.service.UserService;
import org.springframework.context.annotation.Scope;

import java.util.Objects;


/**
 * @author mullerm
 */
@Scope("vaadin-ui")
@SpringView(name = RegistrationView.VIEW_NAME)
public class RegistrationView extends FormLayout implements View {

    public static final String VIEW_NAME = "register";

    private UserService userService;

    private Binder<User> binder;

    public RegistrationView(UserService userService) {
        this.userService = userService;

        setMargin(true);
        initHeader();
        setupBinder();
    }

    private void initHeader(){
        Label register = new Label("Új felhasználó regisztálása");
        register.setStyleName(ValoTheme.LABEL_H3);
        addComponent(register);
    }

    private void setupBinder(){
        binder = new Binder<>();

        TextField nameField = new TextField("Név:");
        nameField.focus();
        binder.forField(nameField).asRequired("Kötelező!").withValidator(name -> name.length() >= 6,
                "Minimum 6 karakter!").bind(User::getName, User::setName);
        nameField.setIcon(VaadinIcons.TEXT_LABEL);
        nameField.setWidth(250, Unit.PIXELS);
        nameField.setPlaceholder("Vezetéknév Keresztnév");

        ComboBox<String> provinceField = new ComboBox<>("Megye:");
        binder.forField(provinceField).asRequired("Kötelező!").bind(User::getProvince, User::setProvince);
        provinceField.setItems(Province.labelValues());
        provinceField.setIcon(VaadinIcons.MAP_MARKER);
        provinceField.setWidth(250, Unit.PIXELS);
        provinceField.setPlaceholder("Megye kiválasztása...");

        TextField emailField = new TextField("Email:");
        binder.forField(emailField).asRequired("Kötelező!").withValidator(new EmailValidator("Érvénytelen e-mail cím!")).bind(User::getEmail, User::setEmail);
        emailField.setIcon(VaadinIcons.AT);
        emailField.setWidth(250, Unit.PIXELS);
        emailField.setPlaceholder("valami@valami.hu");

        TextField addressField = new TextField("Lakcím:");
        binder.forField(addressField).asRequired("Kötelező!").bind(User::getAddress, User::setAddress);
        addressField.setIcon(VaadinIcons.HOME);
        addressField.setWidth(250, Unit.PIXELS);
        addressField.setPlaceholder("1133 Budapest, Babér utca 8.");

        RadioButtonGroup<UserType> typeField = new RadioButtonGroup<>("Típus:");
        if (userService.showUsers(UserType.ADMIN).size() == 0) {
            typeField.setItems(UserType.ADMIN);
        } else {
            typeField.setItems(UserType.FINDER, UserType.RESCUER);
        }
        typeField.setItemCaptionGenerator(UserType::getLabel);
        binder.forField(typeField).asRequired("Kötelező!").bind(User::getType, User::setType);
        typeField.setIcon(VaadinIcons.MALE);

        TextField phoneField = new TextField("Telefonszám:");
        binder.forField(phoneField).withValidator(phone -> phone.length() == 12,
                "Formátum: +36xxxxxxxxx").asRequired("Kötelező telefonszámot megadni!").bind(User::getPhone, User::setPhone);
        phoneField.setIcon(VaadinIcons.PHONE);
        phoneField.setWidth(150, Unit.PIXELS);
        phoneField.setPlaceholder("+36xxxxxxxxx");

        PasswordField passwordField = new PasswordField("Jelszó:");
        binder.forField(passwordField).asRequired("Kötelező!").withValidator(pass -> pass.length() >= 6,
                "A jelszó minimum 6 karakter!" ).bind(User::getPassword, User::setPassword);
        passwordField.setIcon(VaadinIcons.PASSWORD);
        passwordField.setWidth(150, Unit.PIXELS);

        PasswordField checkPasswordField = new PasswordField("Jelszó mégegyszer:");
        binder.forField(checkPasswordField).asRequired("Kötelező a megerősítés.").bind(User::getPassword, (person, password) -> {});
        checkPasswordField.setIcon(VaadinIcons.PASSWORD);
        checkPasswordField.setWidth(150, Unit.PIXELS);

        binder.withValidator(Validator.from(user -> {
            if(passwordField.isEmpty() || checkPasswordField.isEmpty()){
                return true;
            } else {
                return Objects.equals(passwordField.getValue(), checkPasswordField.getValue());
            }
        }, "A két jelszónak meg kell egyeznie!"));

        Label validationStatus = new Label();
        binder.setStatusLabel(validationStatus);

        Button registerButton = new Button("Regisztráció");
        registerButton.setStyleName(ValoTheme.BUTTON_PRIMARY);
        registerButton.setEnabled(false);
        registerButton.addClickListener(event -> {
            try{
                userService.add(binder.getBean());
                Notification.show("Sikeres regisztráció!", "Jelentkezz be!",
                        Notification.Type.TRAY_NOTIFICATION).setPosition(Position.TOP_LEFT);
                getUI().getNavigator().navigateTo("/login");
            }catch(IllegalArgumentException e){
                Notification.show("Ez az email cím foglalt!", Notification.Type.ERROR_MESSAGE);
            }
        });
        binder.addStatusChangeListener(event -> registerButton.setEnabled(binder.isValid()));

        addComponents(nameField, provinceField, addressField, emailField, passwordField, checkPasswordField,
                phoneField, typeField, validationStatus, registerButton);
    }


    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent){
        getUI().getPage().setTitle("AnimalSaver - Regisztráció/Bejelentkezés");
        binder.setBean(new User());
    }

}
