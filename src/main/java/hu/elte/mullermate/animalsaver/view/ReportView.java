package hu.elte.mullermate.animalsaver.view;

import com.vaadin.data.Binder;
import com.vaadin.data.Validator;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.FileResource;
import com.vaadin.server.Page;
import com.vaadin.shared.Position;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import hu.elte.mullermate.animalsaver.domain.Animal;
import hu.elte.mullermate.animalsaver.domain.User;
import hu.elte.mullermate.animalsaver.enums.AnimalCategory;
import hu.elte.mullermate.animalsaver.enums.Province;
import hu.elte.mullermate.animalsaver.service.AnimalService;
import hu.elte.mullermate.animalsaver.service.EmailService;
import org.springframework.context.annotation.Scope;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.time.LocalDate;
import java.util.Locale;

/**
 * @author mullerm
 */
@Scope("vaadin-ui")
@SpringView(name = ReportView.VIEW_NAME)
@Transactional
public class ReportView extends FormLayout implements View {

    public static final String VIEW_NAME = "report";

    private final AnimalService animalService;
    private final EmailService emailService;

    private Upload upload;
    private ImageUpLoader receiver = new ImageUpLoader();
    private TextField picture =  new TextField();
    private Binder<Animal> binder;
    private Notification plsLogIn;
    private User reporter;


    public ReportView(AnimalService animalService, EmailService emailService){
        this.animalService = animalService;
        this.emailService = emailService;

        initUser();
        setupLoginNotification();
        initForm();
        initBinder();
    }

    private void initUser(){
        reporter = ((AdminUI) UI.getCurrent()).getUser();
    }

    private void setupLoginNotification(){
        plsLogIn = new Notification ("Jelentkezz be a folytatáshoz!", Notification.Type.ERROR_MESSAGE);
    }

    private void initForm(){
        setMargin(true);
        setHeightUndefined();
        setDefaultComponentAlignment(Alignment.MIDDLE_LEFT);
        Label checkInfo = new Label("Add meg, hol találtad az állatot! A kategóriaválasztással kapcsolatban az információ menüpontban tájékozódhatsz.");
        checkInfo.setSizeUndefined();
        checkInfo.setStyleName(ValoTheme.LABEL_H3);
        addComponent(checkInfo);
    }

    private void initBinder(){
        binder = new Binder<>();

        TextField zipCodeField = new TextField("Irányítószám:");
        zipCodeField.setPlaceholder("1133");
        zipCodeField.setWidth(75, Unit.PIXELS);
        zipCodeField.setIcon(VaadinIcons.INPUT);
        binder.forField(zipCodeField).withValidator(zipCode -> (zipCode.matches("[0-9]+") && zipCode.length() == 4) || zipCode.isEmpty(),
                "Érvénytelen irányítószám!").bind(Animal::getZipCode, Animal::setZipCode);

        ComboBox<String> provinceField = new ComboBox<>("Megye:");
        provinceField.setPlaceholder("Megye kiválasztása...");
        provinceField.setItems(Province.labelValues());
        provinceField.setWidth(250, Unit.PIXELS);
        provinceField.setIcon(VaadinIcons.MAP_MARKER);
        binder.forField(provinceField).asRequired("Melyik megyében találtad?").bind(Animal::getProvince, Animal::setProvince);

        TextField cityField = new TextField("Település:");
        cityField.setPlaceholder("Budapest");
        cityField.setWidth(200, Unit.PIXELS);
        cityField.setIcon(VaadinIcons.FACTORY);
        binder.forField(cityField).asRequired("Melyik településen találtad?").bind(Animal::getCity, Animal::setCity);

        TextField streetField = new TextField("Utca:");
        streetField.setPlaceholder("Dráva utca");
        streetField.setWidth(200, Unit.PIXELS);
        streetField.setIcon(VaadinIcons.ROAD);
        binder.forField(streetField).asRequired("Milyen utcában találtad?").bind(Animal::getStreet, Animal::setStreet);

        TextField houseNumberField = new TextField("Házszám:");
        houseNumberField.setPlaceholder("10.");
        houseNumberField.setWidth(75, Unit.PIXELS);
        houseNumberField.setIcon(VaadinIcons.HOME);
        binder.forField(houseNumberField).asRequired("Házszám?").bind(Animal::getHouseNumber, Animal::setHouseNumber);

        CheckBox damaged = new CheckBox("Látszódik sérülés az állaton?");
        damaged.setDescription("Látszódik sérülés az állaton?");
        damaged.setSizeUndefined();
        damaged.setIcon(VaadinIcons.CHECK_CIRCLE);
        binder.forField(damaged).bind(Animal::isDamaged, Animal::setDamaged);

        ComboBox<AnimalCategory> categoryComboBox = new ComboBox<>("Válassz kategóriát!");
        categoryComboBox.focus();
        binder.forField(categoryComboBox).asRequired("Kérlek válassz!").bind(Animal::getCategory, Animal::setCategory);
        categoryComboBox.setIcon(VaadinIcons.LIST_SELECT);
        categoryComboBox.setWidth(250, Unit.PIXELS);
        categoryComboBox.setItems(AnimalCategory.values());
        categoryComboBox.setItemCaptionGenerator(AnimalCategory::getLabel);

        upload = new Upload("Tölts fel egy képet!", receiver);
        upload.addSucceededListener(receiver);
        upload.setImmediateMode(true);
        upload.setButtonCaption("Kép kiválasztása..");
        upload.setWidth(200, Unit.PIXELS);
        upload.setIcon(VaadinIcons.FILE_PICTURE);

        picture.setVisible(false);
        binder.forField(picture).bind(Animal::getPicturePath, Animal::setPicturePath);

        DateField date = new DateField("Mikor találtad?");
        date.setIcon(VaadinIcons.CALENDAR);
        date.setPlaceholder(LocalDate.now().toString());
        date.setRangeEnd(LocalDate.now());
        date.setWidth(200, Unit.PIXELS);
        date.setLocale(new Locale("hu", "HU"));
        binder.forField(date).asRequired("Mikor találtad?").bind(Animal::getCreatedTime, Animal::setCreatedTime);

        TextArea commentArea = new TextArea("Megjegyzés:");
        binder.forField(commentArea).bind(Animal::getComment, Animal::setComment);
        commentArea.setWidth(250, Unit.PIXELS);
        commentArea.setIcon(VaadinIcons.COMMENT);
        commentArea.setPlaceholder("A helyszínen hagytad?\n" + "Meddig tudsz rá vigyázni?");
        commentArea.setWordWrap(true);

        binder.withValidator(Validator.from(user -> !cityField.isEmpty()
                        &&!streetField.isEmpty()
                        &&!houseNumberField.isEmpty()
                , "A helyszínt kötelező megadni!"));

        Label validationStatus = new Label();
        binder.setStatusLabel(validationStatus);

        Button reportButton = new Button("Bejelent");
        reportButton.setIcon(VaadinIcons.UPLOAD);
        reportButton.setWidth(200, Unit.PIXELS);
        reportButton.setStyleName(ValoTheme.BUTTON_PRIMARY);
        reportButton.setEnabled(false);
        reportButton.addClickListener(event -> {
            try{
                animalService.reportAnimal(reporter, binder.getBean());
                new Thread(() -> emailService.sendReportMessage(reporter)).start();
                getUI().getNavigator().navigateTo(ReportedView.VIEW_NAME);
                Notification.show("Sikeres bejelentés!", "Köszönjük!",
                        Notification.Type.TRAY_NOTIFICATION).setPosition(Position.TOP_LEFT);
            } catch (IllegalArgumentException e){
                Notification.show("Nincs jogosultságod a bejelentéshez!", Notification.Type.ERROR_MESSAGE);
            }
        });
        binder.addStatusChangeListener(event -> reportButton.setEnabled(binder.isValid()));
        addComponents(categoryComboBox, provinceField, cityField, streetField, houseNumberField, zipCodeField, damaged,
                upload, date, commentArea, validationStatus, reportButton);
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent){
        getUI().getPage().setTitle("AnimalSaver - Bejelentés");
        upload.setEnabled(true);
        binder.setBean(new Animal());
        if(reporter != null){
            binder.getBean().setFinder(reporter);
            binder.getBean().setFinderName(reporter.getName());
        } else {
            plsLogIn.show(Page.getCurrent());
        }
    }


    //IMAGE UPLOAD
    public class ImageUpLoader implements Upload.Receiver, Upload.SucceededListener {

        File file;

        @Override
        public OutputStream receiveUpload(String fileName, String mimeType){
            FileOutputStream fos = null;
            if(reporter != null) {
                if (mimeType.equals("image/jpeg") || mimeType.equals("image/png")) {
                    try {
                        file = new File("src\\main\\resources\\WEB-INF\\reportedIMG\\" + fileName);
                        fos = new FileOutputStream(file);
                        picture.setValue("src/main/resources/WEB-INF/reportedImg/"+fileName);
                        Notification success = new Notification("Sikeres képfeltöltés!", Notification.Type.HUMANIZED_MESSAGE);
                        success.setDelayMsec(2222);
                        success.show(Page.getCurrent());
                    } catch (FileNotFoundException e) {
                        Notification.show("A fájlt nem sikerült megnyitni.", e.getMessage(), Notification.Type.ERROR_MESSAGE);
                        return null;
                    }
                } else {
                    Notification.show("Nem támogatott fájltípus!", Notification.Type.ERROR_MESSAGE);
                }
            } else {
                Notification.show("Jelentkezz be a bejelentéshez!", Notification.Type.ERROR_MESSAGE);
            }
            return fos;
        }

        @Override
        public void uploadSucceeded(Upload.SucceededEvent succeededEvent){
            Image image = new Image("Feltöltött kép");
            image.setSource(new FileResource(file));
            upload.setEnabled(false);
        }
    }


}
