package hu.elte.mullermate.animalsaver.view;

import com.vaadin.data.provider.Query;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.FileResource;
import com.vaadin.shared.data.sort.SortDirection;
import com.vaadin.shared.ui.ValueChangeMode;
import com.vaadin.shared.ui.grid.HeightMode;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.*;
import com.vaadin.ui.components.grid.FooterRow;
import com.vaadin.ui.components.grid.HeaderRow;
import com.vaadin.ui.themes.ValoTheme;
import hu.elte.mullermate.animalsaver.domain.Animal;
import hu.elte.mullermate.animalsaver.enums.AnimalCategory;
import hu.elte.mullermate.animalsaver.enums.AnimalState;
import hu.elte.mullermate.animalsaver.enums.Province;
import hu.elte.mullermate.animalsaver.service.AnimalService;
import org.springframework.context.annotation.Scope;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.util.List;
import java.util.stream.Stream;

import static hu.elte.mullermate.animalsaver.enums.AnimalCategory.getEnumValue;
import static hu.elte.mullermate.animalsaver.enums.AnimalCategory.getLabel;

/**
 * @author mullerm
 */
@Scope("vaadin-ui")
@SpringView(name = ReportedView.VIEW_NAME)
@Transactional
public class ReportedView extends VerticalLayout implements View {

    public static final String VIEW_NAME = "reported";

    private AnimalService animalService;

    private Grid<Animal> animals;
    private FooterRow footer;
    private final ComboBox<String> provinceFilter = new ComboBox<>();
    private final TextField cityFilter = new TextField();
    private final ComboBox<String> categoryFilter = new ComboBox<>();
    private final ComboBox<String> statusFilter = new ComboBox<>();


    public ReportedView(AnimalService animalService){
        this.animalService = animalService;

        setHeightUndefined();
        setDefaultComponentAlignment(Alignment.TOP_CENTER);
        initGrid();
        initFilter();
    }

    private void initGrid(){
        animals = new Grid<>();
        animals.setHeightMode(HeightMode.UNDEFINED);
        animals.setWidth(100, Unit.PERCENTAGE);

        animals.addComponentColumn(animal -> {
            if(animal.getPicturePath() != null){
                FileResource resource = new FileResource(new File(animal.getPicturePath()));
                Image image = new Image("Feltöltött kép", resource);
                Image full = new Image("Teljes méretű kép", resource);
                image.addClickListener(click -> openInFullSize(full));
                image.setWidth(100, Unit.PIXELS);
                image.setHeight(100, Unit.PIXELS);
                return image;
            }else
                return new Label("Nincs kép");
        }).setCaption("Kép").setId("picture");
        animals.addColumn(Animal::getCity).setCaption("Város").setId("city");
        animals.addColumn(Animal::getStreet).setCaption("Közterület").setId("street");
        animals.addColumn(Animal::getHouseNumber).setCaption("Hsz.");
        animals.addComponentColumn(animal -> new Label(getLabel(animal.getCategory()))).setCaption("Kategória").setId("category");
        animals.addComponentColumn(animal -> {
            CheckBox damaged = new CheckBox();
            damaged.setReadOnly(true);
            if (animal.isDamaged()) {
                damaged.setValue(true);
                return damaged;
            } else {
                damaged.setValue(false);
                return damaged;
            }
        }).setCaption("Sérült").setId("damaged");
        animals.addColumn(Animal::getComment).setCaption("Megjegyzés");
        animals.addColumn(Animal::getCreatedTime).setCaption("Időpont").setId("date");
        animals.addComponentColumn(animal -> {
            switch(animal.getState()){
                case FOUND:
                    return new Label("ÚJ");
                case CARE:
                    return new Label("GONDOZVA");
                case RELEASED:
                    return new Label("ELENGEDVE");
                case DEAD:
                    return new Label("ELPUSZTULT");
            }
            return new Label("ISMERETLEN");
        }).setCaption("Státusz").setId("state");
        animals.sort("date", SortDirection.DESCENDING);
        animals.setStyleGenerator(item -> "v-align-center");
        footer = animals.appendFooterRow();
        footer.getCell("picture").setText("Eddig bejelentve:");
        animals.setBodyRowHeight(100);
        addComponent(animals);
    }

    private void initFilter(){
        HeaderRow filterHeader = animals.appendHeaderRow();
        cityFilter.setPlaceholder("Keresés városra...");
        cityFilter.addValueChangeListener(e -> searchForAllFilters());
        cityFilter.setStyleName(ValoTheme.TEXTFIELD_TINY);
        cityFilter.setWidthUndefined();
        cityFilter.setValueChangeMode(ValueChangeMode.LAZY);
        provinceFilter.setItems(Province.labelValues());
        provinceFilter.addValueChangeListener(e -> searchForAllFilters());
        provinceFilter.setStyleName(ValoTheme.COMBOBOX_TINY);
        provinceFilter.setPlaceholder("Szűrés megyére...");
        categoryFilter.setItems(AnimalCategory.labelValues());
        categoryFilter.setPlaceholder("Szűrés kategóriára...");
        categoryFilter.addValueChangeListener(e -> searchForAllFilters());
        categoryFilter.setStyleName(ValoTheme.COMBOBOX_TINY);
        statusFilter.setItems(AnimalState.labelValues().stream().filter(l -> !l.equals("")));
        statusFilter.setPlaceholder("Szűrés státuszra...");
        statusFilter.addValueChangeListener(e -> searchForAllFilters());
        statusFilter.setStyleName(ValoTheme.COMBOBOX_TINY);
        Button clearFiltersButton = new Button("Összes szűrés törlése");
        clearFiltersButton.setStyleName(ValoTheme.BUTTON_TINY);
        clearFiltersButton.addClickListener(e -> clearFilters());
        filterHeader.getCell("picture").setComponent(provinceFilter);
        filterHeader.getCell("city").setComponent(cityFilter);
        filterHeader.getCell("category").setComponent(categoryFilter);
        filterHeader.getCell("damaged").setComponent(clearFiltersButton);
        filterHeader.getCell("state").setComponent(statusFilter);
    }

    private void searchForAllFilters() {
        AnimalCategory category = getEnumValue(categoryFilter.getSelectedItem().orElse(null));
        AnimalState state = AnimalState.getEnumValue(statusFilter.getValue());
        List<Animal> filtered = animalService.allFilters(provinceFilter.getSelectedItem().orElse(null), cityFilter.getValue(), state, category);
        if (filtered.size() > 0)
            animals.setItems(filtered);
        else {
            animals.setItems(Stream.empty());
            Notification.show("A szűrés nem adott eredményt.", Notification.Type.WARNING_MESSAGE);
        }
    }

    private void clearFilters(){
        provinceFilter.clear();
        cityFilter.clear();
        categoryFilter.clear();
        statusFilter.clear();
    }

    private void openInFullSize(Image full){
        VerticalLayout windowContent = new VerticalLayout();
        windowContent.addComponent(full);
        Window fullSizeImage = new Window();
        fullSizeImage.setContent(windowContent);
        fullSizeImage.setSizeUndefined();
        fullSizeImage.center();
        fullSizeImage.setModal(true);
        getUI().addWindow(fullSizeImage);
    }

    private void updateGrid(){
        animals.getDataProvider().refreshAll();
        animals.setItems(animalService.showAll());
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent){
        getUI().getPage().setTitle("AnimalSaver - Bejelentett állatok");
        clearFilters();
        updateGrid();
        if(animals.getDataProvider().size(new Query<>()) == 0)
            Notification.show("Még nem jelentettek be egy állatot sem.", Notification.Type.WARNING_MESSAGE);
        footer.getCell("city").setText(animalService.showAll().size() + " db");
    }

}
