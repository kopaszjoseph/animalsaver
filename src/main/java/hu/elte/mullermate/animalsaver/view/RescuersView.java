package hu.elte.mullermate.animalsaver.view;

import com.vaadin.data.provider.Query;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.Page;
import com.vaadin.shared.Position;
import com.vaadin.shared.data.sort.SortDirection;
import com.vaadin.shared.ui.ValueChangeMode;
import com.vaadin.shared.ui.grid.HeightMode;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.*;
import com.vaadin.ui.components.grid.HeaderRow;
import com.vaadin.ui.themes.ValoTheme;
import hu.elte.mullermate.animalsaver.domain.User;
import hu.elte.mullermate.animalsaver.enums.Province;
import hu.elte.mullermate.animalsaver.enums.UserType;
import hu.elte.mullermate.animalsaver.service.EmailService;
import hu.elte.mullermate.animalsaver.service.UserService;
import org.springframework.context.annotation.Scope;

import java.text.DecimalFormat;
import java.util.List;
import java.util.stream.Stream;

/**
 * @author mullerm
 */
@Scope("vaadin-ui")
@SpringView(name = RescuersView.VIEW_NAME)
public class RescuersView extends VerticalLayout implements View {

    public static final String VIEW_NAME = "rescuers";

    private final UserService userService;
    private final EmailService emailService;
    private final DecimalFormat df = new DecimalFormat("##.##");

    private User user;
    private Grid<User> rescuers;
    private final TextField nameFilter = new TextField();
    private final ComboBox<String> provinceFilter = new ComboBox<>();


    public RescuersView(UserService userService, EmailService emailService){
        this.userService = userService;
        this.emailService = emailService;

        initUser();
        setHeightUndefined();
        setDefaultComponentAlignment(Alignment.TOP_CENTER);
        initGrid();
        initFilter();
    }

    private void initUser(){
        user = ((AdminUI) UI.getCurrent()).getUser();
    }

    private void initGrid(){
        rescuers = new Grid<>();
        rescuers.setHeightMode(HeightMode.UNDEFINED);
        rescuers.setWidth(100, Unit.PERCENTAGE);
        rescuers.addColumn(User::getName).setCaption("Név").setId("name");
        rescuers.addColumn(User::getCaredCount).setCaption("Eddig gondozott állatok száma").setId("cared");
        rescuers.addColumn(User::getPhone).setCaption("Telefonszám").setId("phone");
        rescuers.addColumn(this::addRating).setCaption("Túlélési arány %").setId("rate");
        rescuers.sort("rate", SortDirection.DESCENDING);
        rescuers.addComponentColumn(this::buildContactButton).setCaption("Üzenet");
        rescuers.setStyleGenerator(item -> "v-align-center");
        addComponent(rescuers);
    }

    private String addRating(User user){
        return userService.getCaredCount(user) > 0 ? df.format(userService.rating(user))+"%" : "Még nem elérhető";
    }

    private Button buildContactButton(User rescuer) {
        Button button = new Button("e-mail");
        button.addStyleName(ValoTheme.BUTTON_FRIENDLY);
        button.setIcon(VaadinIcons.ENVELOPE);
        button.addClickListener(event -> openEmailWindow(rescuer));
        return button;
    }

    private void initFilter(){
        HeaderRow filteringHeader = rescuers.appendHeaderRow();
        nameFilter.setPlaceholder("Keresés névre...");
        nameFilter.setStyleName(ValoTheme.TEXTFIELD_TINY);
        nameFilter.addValueChangeListener(e -> searchForAllFilters());
        nameFilter.setValueChangeMode(ValueChangeMode.LAZY);
        provinceFilter.setPlaceholder("Szűrés megyére...");
        provinceFilter.setItems(Province.labelValues());
        provinceFilter.setStyleName(ValoTheme.COMBOBOX_TINY);
        provinceFilter.addValueChangeListener(e -> searchForAllFilters());

        Button clearFiltersButton = new Button("Összes szűrés törlése");
        clearFiltersButton.setStyleName(ValoTheme.BUTTON_TINY);
        clearFiltersButton.addClickListener(e -> clearFilters());
        filteringHeader.getCell("name").setComponent(nameFilter);
        filteringHeader.getCell("cared").setComponent(provinceFilter);
        filteringHeader.getCell("phone").setComponent(clearFiltersButton);
    }

    private void searchForAllFilters(){
        List<User> filtered = userService.allFilters(nameFilter.getValue(), provinceFilter.getSelectedItem().orElse(null));
        if(filtered.size() > 0)
            rescuers.setItems(filtered);
        else {
            rescuers.setItems(Stream.empty());
            Notification.show("A keresés nem adott eredményt.", Notification.Type.WARNING_MESSAGE).setDelayMsec(2000);
        }
    }

    private void clearFilters(){
        nameFilter.clear();
        provinceFilter.clear();
    }

    private void openEmailWindow(User rescuer) {
        Window emailWindow = new Window(" E-mail küldés");
        emailWindow.setIcon(VaadinIcons.PENCIL);
        Label header = new Label("Vedd fel a kapcsolatot a felhasználóval!");
        Label emailAddress = new Label();
        TextArea emailContent = new TextArea();
        Button send = new Button("Küldés");
        Button cancel = new Button("Mégsem");
        emailWindow.setModal(true);
        emailContent.clear();
        emailContent.setPlaceholder("Írj üzenetet!");
        emailContent.setWidth(100, Unit.PERCENTAGE);
        send.setStyleName(ValoTheme.BUTTON_PRIMARY);
        send.addClickListener(event -> {
            new Thread(() -> emailService.sendCustomMessage(user, rescuer, emailContent.getValue())).start();
            emailWindow.close();
            Notification.show("Üzenet elküldve!",
                    Notification.Type.TRAY_NOTIFICATION).setPosition(Position.TOP_LEFT);
        } );
        cancel.addClickListener(e -> emailWindow.close());
        HorizontalLayout buttons = new HorizontalLayout(cancel, send);
        emailAddress.setValue(rescuer.getEmail());
        emailAddress.setStyleName(ValoTheme.LABEL_BOLD);
        VerticalLayout windowLayout = new VerticalLayout(header, emailAddress, emailContent, buttons);
        windowLayout.setComponentAlignment(buttons, Alignment.BOTTOM_RIGHT);
        emailWindow.setContent(windowLayout);
        emailWindow.center();
        emailWindow.setWidth(700, Unit.PIXELS);
        emailWindow.setHeight(350, Unit.PIXELS);
        getUI().addWindow(emailWindow);
    }

    private void updateGrid(){
        rescuers.setItems(userService.showUsers(UserType.RESCUER));
        rescuers.getDataProvider().refreshAll();
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent){
        getUI().getPage().setTitle("AnimalSaver - Állatmentők");
        if(user == null){
            Notification plsLogIn = new Notification("Jelentkezz be a folytatáshoz,", "vagy regisztrálj!",
                    Notification.Type.ERROR_MESSAGE);
            plsLogIn.show(Page.getCurrent());
        } else {
            clearFilters();
            updateGrid();
            if(rescuers.getDataProvider().size(new Query<>()) == 0)
                Notification.show("Még nem regisztrált egy állatmentő sem.",
                        Notification.Type.WARNING_MESSAGE).setDelayMsec(2000);
        }
    }


}
