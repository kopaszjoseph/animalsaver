package hu.elte.mullermate.animalsaver;

import hu.elte.mullermate.animalsaver.service.AnimalHospitalService;
import hu.elte.mullermate.animalsaver.service.AnimalService;
import hu.elte.mullermate.animalsaver.service.EmailService;
import hu.elte.mullermate.animalsaver.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AnimalSaverApplicationTests {

	@Autowired
	private UserDetailsService userDetailsService;

	@Autowired
	private AnimalHospitalService animalHospitalService;

	@Autowired
	private AnimalService animalService;

	@Autowired
	private EmailService emailService;

	@Autowired
	private UserService userService;


	@Test
	public void contextLoads() {
		assertAll(
				() -> assertThat(userDetailsService).isNotNull(),
				() -> assertThat(animalHospitalService).isNotNull(),
				() -> assertThat(animalService).isNotNull(),
				() -> assertThat(emailService).isNotNull(),
				() -> assertThat(userService).isNotNull()
		);
	}

}
