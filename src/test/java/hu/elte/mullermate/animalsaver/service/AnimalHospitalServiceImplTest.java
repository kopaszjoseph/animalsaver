package hu.elte.mullermate.animalsaver.service;

import hu.elte.mullermate.animalsaver.domain.AnimalHospital;
import hu.elte.mullermate.animalsaver.repository.AnimalHospitalRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class AnimalHospitalServiceImplTest {

    @InjectMocks
    private AnimalHospitalServiceImpl service;

    @Mock
    private AnimalHospitalRepository animalHospitalRepository;

    @Test
    void add_should_throw_NullPointerException_if_null_hospital_would_be_saved() {
        //GIVEN
        AnimalHospital hospital = null;

        assertThatThrownBy(
                //WHEN
                () -> service.add(hospital))
                //THEN
                .isInstanceOf(NullPointerException.class);

    }

    @Test
    void add_should_delegate_to_repository_and_save_hospital() {
        //GIVEN
        AnimalHospital hospital = new AnimalHospital();
        given(animalHospitalRepository.save(any(AnimalHospital.class))).willAnswer(invocationOnMock -> invocationOnMock.getArgument(0));

        //WHEN
        service.add(hospital);

        //THEN
        verify(animalHospitalRepository, times(1)).save(hospital);
    }

    @Test
    void showAll_should_delegate_to_repository_and_return_all_saved_hospitals() {
        //GIVEN
        AnimalHospital hospital1 = new AnimalHospital();
        hospital1.setId(1L);
        hospital1.setName("test-name-1");
        hospital1.setAddress("test-address-1");
        hospital1.setCity("test-city-1");
        hospital1.setEmail("test-email-1");
        hospital1.setProvince("test-province-1");
        hospital1.setPhone("test-phone-1");
        hospital1.setWebsite("test-website-1");

        AnimalHospital hospital2 = new AnimalHospital();
        hospital2.setId(2L);
        hospital2.setName("test-name-2");
        hospital2.setAddress("test-address-2");
        hospital2.setCity("test-city-2");
        hospital2.setEmail("test-email-2");
        hospital2.setProvince("test-province-2");
        hospital2.setPhone("test-phone-2");
        hospital2.setWebsite("test-website-2");

        List<AnimalHospital> savedAnimalHospitals = new ArrayList<>();
        savedAnimalHospitals.add(hospital1);
        savedAnimalHospitals.add(hospital2);

        given(animalHospitalRepository.findAll()).willReturn(savedAnimalHospitals);

        //WHEN
        List<AnimalHospital> hospitals = service.showAll();

        //THEN
        verify(animalHospitalRepository, times(1)).findAll();
        assertThat(hospitals.size()).isEqualTo(2);
        assertAll(
                () -> assertThat(hospitals.get(0).getId()).isEqualTo(1L),
                () -> assertThat(hospitals.get(0).getName()).isEqualTo("test-name-1"),
                () -> assertThat(hospitals.get(0).getAddress()).isEqualTo("test-address-1"),
                () -> assertThat(hospitals.get(0).getCity()).isEqualTo("test-city-1"),
                () -> assertThat(hospitals.get(0).getEmail()).isEqualTo("test-email-1"),
                () -> assertThat(hospitals.get(0).getProvince()).isEqualTo("test-province-1"),
                () -> assertThat(hospitals.get(0).getPhone()).isEqualTo("test-phone-1"),
                () -> assertThat(hospitals.get(0).getWebsite()).isEqualTo("test-website-1"),
                () -> assertThat(hospitals.get(1).getId()).isEqualTo(2L),
                () -> assertThat(hospitals.get(1).getName()).isEqualTo("test-name-2"),
                () -> assertThat(hospitals.get(1).getAddress()).isEqualTo("test-address-2"),
                () -> assertThat(hospitals.get(1).getCity()).isEqualTo("test-city-2"),
                () -> assertThat(hospitals.get(1).getEmail()).isEqualTo("test-email-2"),
                () -> assertThat(hospitals.get(1).getProvince()).isEqualTo("test-province-2"),
                () -> assertThat(hospitals.get(1).getPhone()).isEqualTo("test-phone-2"),
                () -> assertThat(hospitals.get(1).getWebsite()).isEqualTo("test-website-2")
        );
    }

    @Test
    void close_should_delete_hospital_from_repository() {
        //GIVEN
        AnimalHospital hospital = new AnimalHospital();

        //WHEN
        service.close(hospital);

        //THEN
        verify(animalHospitalRepository, times(1)).delete(hospital);
    }

    @Test
    void allFilters_should_return_filtered_hospitals() {
        //GIVEN
        AnimalHospital hospital1 = new AnimalHospital();
        hospital1.setId(1L);
        hospital1.setName("test-name-1");
        hospital1.setAddress("test-address-1");
        hospital1.setCity("test-city-1");
        hospital1.setEmail("test-email-1");
        hospital1.setProvince("test-province-1");
        hospital1.setPhone("test-phone-1");
        hospital1.setWebsite("test-website-1");

        AnimalHospital hospital2 = new AnimalHospital();
        hospital2.setId(2L);
        hospital2.setName("test-name-2");
        hospital2.setAddress("test-address-2");
        hospital2.setCity("test-city-2");
        hospital2.setEmail("test-email-2");
        hospital2.setProvince("test-province-2");
        hospital2.setPhone("test-phone-2");
        hospital2.setWebsite("test-website-2");

        List<AnimalHospital> savedAnimalHospitals = new ArrayList<>();
        savedAnimalHospitals.add(hospital1);
        savedAnimalHospitals.add(hospital2);

        given(animalHospitalRepository.findAll()).willReturn(savedAnimalHospitals);

        //WHEN
        List<AnimalHospital> filtered = service.allFilters("test-province-1", "test-city-1", "");

        //THEN
        verify(animalHospitalRepository, times(1)).findAll();
        assertThat(filtered.size()).isEqualTo(1);

        assertAll(
                () -> assertThat(filtered.get(0).getId()).isEqualTo(1L),
                () -> assertThat(filtered.get(0).getName()).isEqualTo("test-name-1"),
                () -> assertThat(filtered.get(0).getAddress()).isEqualTo("test-address-1"),
                () -> assertThat(filtered.get(0).getCity()).isEqualTo("test-city-1"),
                () -> assertThat(filtered.get(0).getEmail()).isEqualTo("test-email-1"),
                () -> assertThat(filtered.get(0).getProvince()).isEqualTo("test-province-1"),
                () -> assertThat(filtered.get(0).getPhone()).isEqualTo("test-phone-1"),
                () -> assertThat(filtered.get(0).getWebsite()).isEqualTo("test-website-1")
        );
    }

    @Test
    void deleteAll_should_delete_all_hospitals_from_repository() {
        //WHEN
        service.deleteAll();

        //THEN
        verify(animalHospitalRepository, times(1)).deleteAll();
    }
}
