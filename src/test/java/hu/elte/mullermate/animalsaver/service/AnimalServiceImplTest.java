package hu.elte.mullermate.animalsaver.service;

import hu.elte.mullermate.animalsaver.domain.Animal;
import hu.elte.mullermate.animalsaver.domain.User;
import hu.elte.mullermate.animalsaver.enums.AnimalState;
import hu.elte.mullermate.animalsaver.enums.UserType;
import hu.elte.mullermate.animalsaver.repository.AnimalRepository;
import hu.elte.mullermate.animalsaver.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class AnimalServiceImplTest {

    @InjectMocks
    private AnimalServiceImpl service;

    @Mock
    private AnimalRepository animalRepository;

    @Mock
    private UserRepository userRepository;


    @Test
    void changeState_should_throw_NullPointerException_if_null_animal_would_be_saved() {
        //GIVEN
        Animal animal = null;

        assertThatThrownBy(
                //WHEN
                () -> service.changeState(animal, AnimalState.NEW))
                //THEN
                .isInstanceOf(NullPointerException.class);
    }

    @Test
    void changeState_should_throw_NullPointerException_if_null_state_would_be_saved() {
        //GIVEN
        Animal animal = new Animal();
        AnimalState state = null;

        assertThatThrownBy(
                //WHEN
                () -> service.changeState(animal, state))
                //THEN
                .isInstanceOf(NullPointerException.class);
    }

    @Test
    void changeState_should_throw_IllegalArgumentException_if_same_state_would_be_saved() {
        //GIVEN
        Animal animal = new Animal();
        AnimalState state = AnimalState.NEW;
        animal.setState(state);

        assertThatThrownBy(
                //WHEN
                () -> service.changeState(animal, AnimalState.NEW))
                //THEN
                .isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    void changeState_should_delegate_to_repository_and_save_animal_if_state_was_modified() {
        //GIVEN
        Animal animal = new Animal();
        AnimalState state = AnimalState.NEW;
        animal.setState(state);

        given(animalRepository.save(any(Animal.class))).willAnswer(invocationOnMock -> invocationOnMock.getArgument(0));

        //WHEN
        service.changeState(animal, AnimalState.FOUND);

        //THEN
        verify(animalRepository, times(1)).save(animal);
        assertThat(animal.getState()).isEqualTo(AnimalState.FOUND);
    }

    @Test
    void showAll_should_delegate_to_repository_and_return_all_saved_animals() {
        //GIVEN
        Animal animal1 = new Animal();
        animal1.setFinderName("test-finder-name-1");
        animal1.setCity("test-city-1");
        animal1.setStreet("test-street-1");
        animal1.setHouseNumber("test-house-number-1");

        Animal animal2 = new Animal();
        animal2.setFinderName("test-finder-name-2");
        animal2.setCity("test-city-2");
        animal2.setStreet("test-street-2");
        animal2.setHouseNumber("test-house-number-2");

        List<Animal> savedAnimals = new ArrayList<>();
        savedAnimals.add(animal1);
        savedAnimals.add(animal2);

        given(animalRepository.findAll()).willReturn(savedAnimals);

        //WHEN
        List<Animal> animals = service.showAll();

        //THEN
        verify(animalRepository, times(1)).findAll();
        assertAll(
                () -> assertThat(animals.size()).isEqualTo(2),
                () -> assertThat(animals.get(0).getFinderName()).isEqualTo("test-finder-name-1"),
                () -> assertThat(animals.get(0).getCity()).isEqualTo("test-city-1"),
                () -> assertThat(animals.get(0).getStreet()).isEqualTo("test-street-1"),
                () -> assertThat(animals.get(0).getHouseNumber()).isEqualTo("test-house-number-1"),
                () -> assertThat(animals.get(1).getFinderName()).isEqualTo("test-finder-name-2"),
                () -> assertThat(animals.get(1).getCity()).isEqualTo("test-city-2"),
                () -> assertThat(animals.get(1).getStreet()).isEqualTo("test-street-2"),
                () -> assertThat(animals.get(1).getHouseNumber()).isEqualTo("test-house-number-2")
        );
    }

    @Test
    void allFilters_should_return_filtered_animals() {
        //GIVEN
        Animal animal1 = new Animal();
        animal1.setFinderName("test-finder-name-1");
        animal1.setProvince("test-province-1");
        animal1.setCity("test-city-1");
        animal1.setStreet("test-street-1");
        animal1.setHouseNumber("test-house-number-1");
        animal1.setState(AnimalState.NEW);

        Animal animal2 = new Animal();
        animal2.setFinderName("test-finder-name-2");
        animal2.setProvince("test-province-2");
        animal2.setCity("test-city-2");
        animal2.setStreet("test-street-2");
        animal2.setHouseNumber("test-house-number-2");
        animal2.setState(AnimalState.NEW);


        List<Animal> savedAnimals = new ArrayList<>();
        savedAnimals.add(animal1);
        savedAnimals.add(animal2);

        given(animalRepository.findAll()).willReturn(savedAnimals);

        //WHEN
        List<Animal> animals = service.allFilters("", "test-city-1", AnimalState.NEW, null);

        //THEN
        verify(animalRepository, times(1)).findAll();
        assertAll(
                () -> assertThat(animals.size()).isEqualTo(1),
                () -> assertThat(animals.get(0).getFinderName()).isEqualTo("test-finder-name-1"),
                () -> assertThat(animals.get(0).getCity()).isEqualTo("test-city-1"),
                () -> assertThat(animals.get(0).getStreet()).isEqualTo("test-street-1"),
                () -> assertThat(animals.get(0).getHouseNumber()).isEqualTo("test-house-number-1")
        );
    }

    @Test
    void reportAnimal_should_throw_IllegalArgumentException_if_user_is_not_found() {
        //GIVEN
        User user = new User();
        user.setEmail("test-email");
        Animal animal = new Animal();

        given(userRepository.findByEmail("test-email")).willReturn(Optional.empty());

        assertThatThrownBy(
                //WHEN
                () -> service.reportAnimal(user, animal))
                //THEN
                .isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    void reportAnimal_should_throw_IllegalArgumentException_if_user_userType_is_not_correct() {
        //GIVEN
        User user = new User();
        user.setEmail("test-email");
        Animal animal = new Animal();

        given(userRepository.findByEmail("test-email")).willReturn(Optional.empty());

        assertThatThrownBy(
                //WHEN
                () -> service.reportAnimal(user, animal))
                //THEN
                .isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    void reportAnimal_should_delegate_to_repositories_and_save_user_and_animal() {
        //GIVEN
        User user = new User();
        user.setType(UserType.FINDER);
        user.setEmail("test-email");
        user.setAnimals(new ArrayList<>());
        Animal animal = new Animal();

        given(userRepository.findByEmail("test-email")).willReturn(Optional.of(user));
        given(userRepository.save(any(User.class))).willAnswer(invocationOnMock -> invocationOnMock.getArgument(0));
        given(animalRepository.save(any(Animal.class))).willAnswer(invocationOnMock -> invocationOnMock.getArgument(0));

        //WHEN
        service.reportAnimal(user, animal);

        //THEN
        verify(userRepository, times(2)).findByEmail("test-email");
        verify(userRepository, times(1)).save(user);
        verify(animalRepository, times(2)).save(animal);

        assertAll(
                () -> assertThat(animal.getState()).isEqualTo(AnimalState.FOUND),
                () -> assertThat(user.getAnimals().size()).isEqualTo(1)
        );
    }

    @Test
    void careAnimal_should_throw_IllegalArgumentException_if_user_is_not_found() {
        //GIVEN
        User user = new User();
        user.setEmail("test-email");
        Animal animal = new Animal();

        given(userRepository.findByEmail("test-email")).willReturn(Optional.empty());

        assertThatThrownBy(
                //WHEN
                () -> service.careAnimal(user, animal))
                //THEN
                .isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    void careAnimal_should_throw_IllegalArgumentException_if_userType_is_not_correct() {
        //GIVEN
        User user = new User();
        user.setEmail("test-email");
        user.setType(UserType.FINDER);
        Animal animal = new Animal();
        animal.setId(1L);

        given(userRepository.findByEmail("test-email")).willReturn(Optional.of(user));
        given(animalRepository.findById(1L)).willReturn(Optional.of(animal));

        assertThatThrownBy(
                //WHEN
                () -> service.careAnimal(user, animal))
                //THEN
                .isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    void careAnimal_should_delegate_to_repositories_and_save_user_and_animal() {
        //GIVEN
        User user = new User();
        user.setEmail("test-email");
        user.setAnimals(new ArrayList<>());
        user.setType(UserType.RESCUER);
        Animal animal = new Animal();
        animal.setId(1L);

        given(userRepository.findByEmail("test-email")).willReturn(Optional.of(user));
        given(animalRepository.findById(1L)).willReturn(Optional.of(animal));
        given(userRepository.save(any(User.class))).willAnswer(invocationOnMock -> invocationOnMock.getArgument(0));
        given(animalRepository.save(any(Animal.class))).willAnswer(invocationOnMock -> invocationOnMock.getArgument(0));

        //WHEN
        service.careAnimal(user, animal);

        //THEN
        verify(userRepository, times(2)).findByEmail("test-email");
        verify(userRepository, times(1)).save(user);
        verify(animalRepository, times(2)).save(animal);

        assertAll(
                () -> assertThat(animal.getState()).isEqualTo(AnimalState.CARE),
                () -> assertThat(user.getAnimals().size()).isEqualTo(1)

        );
    }

    @Test
    void releaseAnimal_should_throw_IllegalArgumentException_if_user_is_not_found() {
        //GIVEN
        User user = new User();
        user.setEmail("test-email");
        Animal animal = new Animal();

        given(userRepository.findByEmail("test-email")).willReturn(Optional.empty());

        assertThatThrownBy(
                //WHEN
                () -> service.releaseAnimal(user, animal))
                //THEN
                .isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    void releaseAnimal_should_throw_IllegalArgumentException_if_userType_is_not_correct() {
        //GIVEN
        User user = new User();
        user.setEmail("test-email");
        user.setType(UserType.FINDER);
        Animal animal = new Animal();
        animal.setId(1L);

        given(userRepository.findByEmail("test-email")).willReturn(Optional.of(user));
        given(animalRepository.findById(1L)).willReturn(Optional.of(animal));

        assertThatThrownBy(
                //WHEN
                () -> service.releaseAnimal(user, animal))
                //THEN
                .isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    void releaseAnimal_should_delegate_to_repositories_and_save_user_and_animal() {
        //GIVEN
        User user = new User();
        user.setEmail("test-email");
        user.setType(UserType.RESCUER);
        Animal animal = new Animal();
        animal.setId(1L);

        given(userRepository.findByEmail("test-email")).willReturn(Optional.of(user));
        given(animalRepository.findById(1L)).willReturn(Optional.of(animal));
        given(userRepository.save(any(User.class))).willAnswer(invocationOnMock -> invocationOnMock.getArgument(0));
        given(animalRepository.save(any(Animal.class))).willAnswer(invocationOnMock -> invocationOnMock.getArgument(0));

        //WHEN
        service.releaseAnimal(user, animal);

        //THEN
        verify(userRepository, times(2)).findByEmail("test-email");
        verify(userRepository, times(1)).save(user);
        verify(animalRepository, times(2)).save(animal);

        assertAll(
                () -> assertThat(animal.getState()).isEqualTo(AnimalState.RELEASED),
                () -> assertThat((int) user.getReleasedCount()).isEqualTo(1)
        );
    }

    @Test
    void buryAnimal_should_throw_IllegalArgumentException_if_user_is_not_found() {
        //GIVEN
        User user = new User();
        user.setEmail("test-email");
        Animal animal = new Animal();

        given(userRepository.findByEmail("test-email")).willReturn(Optional.empty());

        assertThatThrownBy(
                //WHEN
                () -> service.buryAnimal(user, animal))
                //THEN
                .isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    void buryAnimal_should_throw_IllegalArgumentException_if_userType_is_not_correct() {
        //GIVEN
        User user = new User();
        user.setEmail("test-email");
        user.setType(UserType.FINDER);
        Animal animal = new Animal();
        animal.setId(1L);

        given(userRepository.findByEmail("test-email")).willReturn(Optional.of(user));
        given(animalRepository.findById(1L)).willReturn(Optional.of(animal));

        assertThatThrownBy(
                //WHEN
                () -> service.buryAnimal(user, animal))
                //THEN
                .isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    void buryAnimal_should_delegate_to_repositories_and_save_user_and_animal() {
        //GIVEN
        User user = new User();
        user.setEmail("test-email");
        user.setType(UserType.RESCUER);
        Animal animal = new Animal();
        animal.setId(1L);

        given(userRepository.findByEmail("test-email")).willReturn(Optional.of(user));
        given(animalRepository.findById(1L)).willReturn(Optional.of(animal));
        given(userRepository.save(any(User.class))).willAnswer(invocationOnMock -> invocationOnMock.getArgument(0));
        given(animalRepository.save(any(Animal.class))).willAnswer(invocationOnMock -> invocationOnMock.getArgument(0));

        //WHEN
        service.buryAnimal(user, animal);

        //THEN
        verify(userRepository, times(2)).findByEmail("test-email");
        verify(userRepository, times(1)).save(user);
        verify(animalRepository, times(2)).save(animal);

        assertAll(
                () -> assertThat(animal.getState()).isEqualTo(AnimalState.DEAD),
                () -> assertThat((int) user.getDeadCount()).isEqualTo(1)
        );
    }

    @Test
    void releasedAnimals_should_throw_IllegalArgumentException_if_userType_is_not_correct() {
        //GIVEN
        User user = new User();
        user.setType(UserType.FINDER);

        assertThatThrownBy(
                //WHEN
                () -> service.releasedAnimals(user))
                //THEN
                .isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    void releasedAnimals_should_throw_IllegalArgumentException_if_user_is_not_found() {
        //GIVEN
        User user = new User();
        user.setEmail("test-email");
        user.setType(UserType.RESCUER);

        given(userRepository.findByEmail("test-email")).willReturn(Optional.empty());

        assertThatThrownBy(
                //WHEN
                () -> service.releasedAnimals(user))
                //THEN
                .isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    void releasedAnimals_should_return_number_of_released_animals_for_user() {
        //GIVEN
        User user = new User();
        user.setEmail("test-email");
        user.setType(UserType.RESCUER);
        user.setReleasedCount(2);

        given(userRepository.findByEmail("test-email")).willReturn(Optional.of(user));

        //WHEN
        Double released = service.releasedAnimals(user);

        //THEN
        verify(userRepository, times(1)).findByEmail("test-email");
        assertThat(released).isEqualTo(2);
    }

    @Test
    void deadAnimals_should_throw_IllegalArgumentException_if_userType_is_not_correct() {
        //GIVEN
        User user = new User();
        user.setType(UserType.FINDER);

        assertThatThrownBy(
                //WHEN
                () -> service.deadAnimals(user))
                //THEN
                .isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    void deadAnimals_should_throw_IllegalArgumentException_if_user_is_not_found() {
        //GIVEN
        User user = new User();
        user.setEmail("test-email");
        user.setType(UserType.RESCUER);

        given(userRepository.findByEmail("test-email")).willReturn(Optional.empty());

        assertThatThrownBy(
                //WHEN
                () -> service.deadAnimals(user))
                //THEN
                .isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    void deadAnimals_should_return_number_of_released_animals_for_user() {
        //GIVEN
        User user = new User();
        user.setEmail("test-email");
        user.setType(UserType.RESCUER);
        user.setDeadCount(2);

        given(userRepository.findByEmail("test-email")).willReturn(Optional.of(user));

        //WHEN
        Double deadCount = service.deadAnimals(user);

        //THEN
        verify(userRepository, times(1)).findByEmail("test-email");
        assertThat(deadCount).isEqualTo(2);
    }

    @Test
    void caredAnimals_should_throw_IllegalArgumentException_if_userType_is_not_correct() {
        //GIVEN
        User user = new User();
        user.setType(UserType.FINDER);

        assertThatThrownBy(
                //WHEN
                () -> service.caredAnimals(user))
                //THEN
                .isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    void caredAnimals_should_throw_IllegalArgumentException_if_user_is_not_found() {
        //GIVEN
        User user = new User();
        user.setEmail("test-email");
        user.setType(UserType.RESCUER);

        given(userRepository.findByEmail("test-email")).willReturn(Optional.empty());

        assertThatThrownBy(
                //WHEN
                () -> service.deadAnimals(user))
                //THEN
                .isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    void caredAnimals_should_return_number_of_released_animals_for_user() {
        //GIVEN
        User user = new User();
        user.setEmail("test-email");
        user.setType(UserType.RESCUER);
        user.setCaredCount(2);

        given(userRepository.findByEmail("test-email")).willReturn(Optional.of(user));

        //WHEN
        Double caredCount = service.caredAnimals(user);

        //THEN
        verify(userRepository, times(1)).findByEmail("test-email");
        assertThat(caredCount).isEqualTo(2);
    }

    @Test
    void reportedAnimals_should_throw_IllegalArgumentException_if_userType_is_not_correct() {
        //GIVEN
        User user = new User();
        user.setType(UserType.FINDER);

        assertThatThrownBy(
                //WHEN
                () -> service.reportedAnimals(user))
                //THEN
                .isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    void reportedAnimals_should_throw_IllegalArgumentException_if_user_is_not_found() {
        //GIVEN
        User user = new User();
        user.setEmail("test-email");
        user.setType(UserType.RESCUER);

        given(userRepository.findByEmail("test-email")).willReturn(Optional.empty());

        assertThatThrownBy(
                //WHEN
                () -> service.reportedAnimals(user))
                //THEN
                .isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    void reportedAnimals_should_return_number_of_released_animals_for_user() {
        //GIVEN
        User user = new User();
        user.setEmail("test-email");
        user.setType(UserType.RESCUER);
        user.setReportedCount(2);

        given(userRepository.findByEmail("test-email")).willReturn(Optional.of(user));

        //WHEN
        Double reportedCount = service.reportedAnimals(user);

        //THEN
        verify(userRepository, times(1)).findByEmail("test-email");
        assertThat(reportedCount).isEqualTo(2);
    }


    @Test
    void showReportedAnimals_should_return_all_animals_with_FOUND_state() {
        //GIVEN
        Animal animal1 = new Animal();
        animal1.setState(AnimalState.FOUND);

        Animal animal2 = new Animal();
        animal2.setState(AnimalState.FOUND);

        List<Animal> savedAnimals = new ArrayList<>();
        savedAnimals.add(animal1);
        savedAnimals.add(animal2);

        given(animalRepository.findByState(AnimalState.FOUND)).willReturn(savedAnimals);

        //WHEN
        List<Animal> reportedAnimals = service.showReportedAnimals();

        //THEN
        verify(animalRepository, times(1)).findByState(AnimalState.FOUND);
        assertThat(reportedAnimals.size()).isEqualTo(2);
    }

    @Test
    void healingAnimalsByUser_should_throw_IllegalArgumentException_if_user_is_not_found() {
        //GIVEN
        User user = new User();
        user.setEmail("test-email");

        given(userRepository.findByEmail(anyString())).willReturn(Optional.empty());

        assertThatThrownBy(
                //WHEN
                () -> service.healingAnimalsByUser(user))
                //THEN
                .isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    void healingAnimalsByUser_should_return_empty_list_if_userType_is_not_correct() {
        //GIVEN
        User user = new User();
        user.setEmail("test-email");
        user.setType(UserType.FINDER);

        given(userRepository.findByEmail("test-email")).willReturn(Optional.of(user));

        //WHEN
        List<Animal> animals = service.healingAnimalsByUser(user);

        //THEN
        verify(userRepository, times(1)).findByEmail("test-email");
        assertThat(animals.size()).isEqualTo(0);
    }

    @Test
    void healingAnimalsByUser_should_return_healing_animal_list_if_userType_is_correct() {
        //GIVEN
        User user = new User();
        user.setEmail("test-email");
        user.setType(UserType.RESCUER);

        Animal animal = new Animal();
        animal.setState(AnimalState.CARE);

        List<Animal> savedAnimals = new ArrayList<>();
        savedAnimals.add(animal);

        user.setAnimals(savedAnimals);

        given(userRepository.findByEmail("test-email")).willReturn(Optional.of(user));

        //WHEN
        List<Animal> animals = service.healingAnimalsByUser(user);

        //THEN
        verify(userRepository, times(1)).findByEmail("test-email");
        assertThat(animals.size()).isEqualTo(1);
    }

    @Test
    void healedAnimalsByUser_should_throw_IllegalArgumentException_if_user_is_not_found() {
        //GIVEN
        User user = new User();
        user.setEmail("test-email");

        given(userRepository.findByEmail(anyString())).willReturn(Optional.empty());

        assertThatThrownBy(
                //WHEN
                () -> service.healedAnimalsByUser(user))
                //THEN
                .isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    void healedAnimalsByUser_should_return_healed_animal_list_if_userType_is_correct() {
        //GIVEN
        User user = new User();
        user.setEmail("test-email");
        user.setType(UserType.RESCUER);

        Animal animal1 = new Animal();
        animal1.setState(AnimalState.RELEASED);

        Animal animal2 = new Animal();
        animal2.setState(AnimalState.DEAD);

        List<Animal> savedAnimals = new ArrayList<>();
        savedAnimals.add(animal1);
        savedAnimals.add(animal2);

        user.setAnimals(savedAnimals);

        given(userRepository.findByEmail("test-email")).willReturn(Optional.of(user));

        //WHEN
        List<Animal> animals = service.healedAnimalsByUser(user);

        //THEN
        verify(userRepository, times(1)).findByEmail("test-email");
        assertThat(animals.size()).isEqualTo(2);
    }

    @Test
    void changeDamagedStatus_should_change_animal_damaged_status() {
        //GIVEN
        Animal animal = new Animal();
        animal.setId(1L);
        animal.setDamaged(false);

        given(animalRepository.findById(anyLong())).willReturn(Optional.of(animal));
        given(animalRepository.save(any(Animal.class))).willAnswer(invocationOnMock -> invocationOnMock.getArgument(0));

        //
        service.changeDamagedStatus(animal);

        //THEN
        verify(animalRepository, times(1)).findById(1L);
        verify(animalRepository, times(1)).save(animal);
        assertThat(animal.isDamaged()).isTrue();
    }

    @Test
    void deleteAllAnimals_should_delegate_to_repository_and_delete_all_animals() {

        //WHEN
        service.deleteAllAnimals();

        //THEN
        verify(animalRepository, times(1)).deleteAll();
    }
}
