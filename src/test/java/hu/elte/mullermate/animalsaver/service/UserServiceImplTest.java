package hu.elte.mullermate.animalsaver.service;

import hu.elte.mullermate.animalsaver.domain.Animal;
import hu.elte.mullermate.animalsaver.domain.User;
import hu.elte.mullermate.animalsaver.enums.UserType;
import hu.elte.mullermate.animalsaver.repository.AnimalRepository;
import hu.elte.mullermate.animalsaver.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class UserServiceImplTest {

    @InjectMocks
    private UserServiceImpl service;

    @Mock
    private UserRepository userRepository;

    @Mock
    private AnimalRepository animalRepository;

    @Mock
    private PasswordEncoder passwordEncoder;

    @Test
    void add_should_throw_IllegalArgumentException_if_user_is_null() {
        //GIVEN
        User user = null;

        assertThatThrownBy(
                //WHEN
                () -> service.add(user))
                //THEN
                .isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    void add_should_throw_IllegalArgumentException_if_email_is_already_registered() {
        //GIVEN
        User saved = new User();
        saved.setEmail("test-email");

        User toSave = new User();
        toSave.setEmail("test-email");

        given(userRepository.findAll()).willReturn(Collections.singletonList(saved));

        assertThatThrownBy(
                //WHEN
                () -> service.add(toSave))
                //THEN
                .isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    void add_should_delegate_to_repository_and_save_user() {
        //GIVEN
        User toSave = new User();
        toSave.setEmail("test-email");
        toSave.setType(UserType.RESCUER);
        toSave.setName("test-name");
        toSave.setPassword("test-password");

        given(userRepository.findAll()).willReturn(Collections.emptyList());
        given(passwordEncoder.encode(anyString())).willAnswer(invocationOnMock -> invocationOnMock.getArgument(0));
        given(userRepository.save(any(User.class))).willAnswer(invocationOnMock -> invocationOnMock.getArgument(0));

        //WHEN
        User saved = service.add(toSave);

        //THEN
        verify(userRepository, times(1)).findAll();
        verify(userRepository, times(1)).save(any(User.class));

        assertAll(
                () -> assertThat(toSave.getEmail()).isEqualTo(saved.getEmail()),
                () -> assertThat(toSave.getType()).isEqualTo(saved.getType()),
                () -> assertThat(toSave.getName()).isEqualTo(saved.getName()),
                () -> assertThat(toSave.getPassword()).isEqualTo(saved.getPassword())
        );
    }

    @Test
    void getReportedCount_should_throw_IllegalArgumentException_if_user_is_not_found() {
        //GIVEN
        User user = new User();
        user.setEmail("test-email");

        given(userRepository.findByEmail(anyString())).willReturn(Optional.empty());

        assertThatThrownBy(
                //WHEN
                () -> service.getReportedCount(user))
                //THEN
                .isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    void getReportedCount_should_delegate_to_repository_and_return_number_correctly_if_user_is_found() {
        //GIVEN
        User user = new User();
        user.setEmail("test-email");
        user.setReportedCount(3);

        given(userRepository.findByEmail(anyString())).willReturn(Optional.of(user));

        //WHEN
        double reported = service.getReportedCount(user);

        //THEN
        verify(userRepository, times(1)).findByEmail(anyString());
        assertThat(reported).isEqualTo(user.getReportedCount());

    }

    @Test
    void getCaredCount_should_throw_IllegalArgumentException_if_user_is_not_found() {
        //GIVEN
        User user = new User();
        user.setEmail("test-email");

        given(userRepository.findByEmail(anyString())).willReturn(Optional.empty());

        assertThatThrownBy(
                //WHEN
                () -> service.getCaredCount(user))
                //THEN
                .isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    void getCaredCount_should_delegate_to_repository_and_return_number_correctly_if_user_is_found() {
        //GIVEN
        User user = new User();
        user.setEmail("test-email");
        user.setCaredCount(2);

        given(userRepository.findByEmail(anyString())).willReturn(Optional.of(user));

        //WHEN
        double cared = service.getCaredCount(user);

        //THEN
        verify(userRepository, times(1)).findByEmail(anyString());
        assertThat(cared).isEqualTo(user.getCaredCount());

    }

    @Test
    void getDeadCount_should_throw_IllegalArgumentException_if_user_is_not_found() {
        //GIVEN
        User user = new User();
        user.setEmail("test-email");

        given(userRepository.findByEmail(anyString())).willReturn(Optional.empty());

        assertThatThrownBy(
                //WHEN
                () -> service.getDeadCount(user))
                //THEN
                .isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    void getDeadCount_should_delegate_to_repository_and_return_number_correctly_if_user_is_found() {
        //GIVEN
        User user = new User();
        user.setEmail("test-email");
        user.setCaredCount(2);

        given(userRepository.findByEmail(anyString())).willReturn(Optional.of(user));

        //WHEN
        double dead = service.getDeadCount(user);

        //THEN
        verify(userRepository, times(1)).findByEmail(anyString());
        assertThat(dead).isEqualTo(user.getDeadCount());

    }

    @Test
    void getReleasedCount_should_throw_IllegalArgumentException_if_user_is_not_found() {
        //GIVEN
        User user = new User();
        user.setEmail("test-email");

        given(userRepository.findByEmail(anyString())).willReturn(Optional.empty());

        assertThatThrownBy(
                //WHEN
                () -> service.getReleasedCount(user))
                //THEN
                .isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    void getReleasedCount_should_delegate_to_repository_and_return_number_correctly_if_user_is_found() {
        //GIVEN
        User user = new User();
        user.setEmail("test-email");
        user.setCaredCount(2);

        given(userRepository.findByEmail(anyString())).willReturn(Optional.of(user));

        //WHEN
        double released = service.getReleasedCount(user);

        //THEN
        verify(userRepository, times(1)).findByEmail(anyString());
        assertThat(released).isEqualTo(user.getReleasedCount());

    }

    @Test
    void findByEmail_should_throw_IllegalArgumentException_if_user_is_not_found() {
        //GIVEN
        User user = new User();
        user.setEmail("test-email");

        given(userRepository.findByEmail(anyString())).willReturn(Optional.empty());

        assertThatThrownBy(
                //WHEN
                () -> service.findByEmail(user.getEmail()))
                //THEN
                .isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    void findByEmail_should_delegate_to_repository_and_return_user_if_user_is_found() {
        //GIVEN
        User user = new User();
        user.setEmail("test-email");
        user.setCaredCount(2);

        given(userRepository.findByEmail(anyString())).willReturn(Optional.of(user));

        //WHEN
        User found = service.findByEmail("test-email");

        //THEN
        verify(userRepository, times(1)).findByEmail(anyString());
        assertThat(found.getCaredCount()).isEqualTo(user.getCaredCount());

    }

    @Test
    void updateUser_should_throw_IllegalArgumentException_if_user_is_not_found() {
        //GIVEN
        User user = new User();
        user.setEmail("test-email");

        given(userRepository.findByEmail(anyString())).willReturn(Optional.empty());

        assertThatThrownBy(
                //WHEN
                () -> service.updateUser(user, user))
                //THEN
                .isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    void updateUser_should_delegate_to_repository_and_save_update_data() {
        //GIVEN
        User user = new User();
        user.setEmail("test-email");
        user.setAddress("test-address");

        User update = new User();
        update.setEmail("test-email");
        update.setAddress("updated-address");

        given(userRepository.findByEmail(anyString())).willReturn(Optional.of(user));
        given(userRepository.save(any(User.class))).willAnswer(invocationOnMock -> invocationOnMock.getArgument(0));

        //WHEN
        service.updateUser(user, update);

        //THEN
        verify(userRepository, times(1)).findByEmail(anyString());
        verify(userRepository, times(1)).save(any(User.class));
        assertThat(user.getAddress()).isEqualTo("updated-address");
    }

    @Test
    void updatePassword_should_throw_IllegalArgumentException_if_user_is_not_found() {
        //GIVEN
        User user = new User();
        user.setEmail("test-email");

        given(userRepository.findByEmail(anyString())).willReturn(Optional.empty());

        assertThatThrownBy(
                //WHEN
                () -> service.updatePassword(user, user))
                //THEN
                .isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    void updatePassword_should_delegate_to_repository_and_save_update_data() {
        //GIVEN
        User user = new User();
        user.setEmail("test-email");
        user.setAddress("test-address");
        user.setPassword("test-password");

        User update = new User();
        update.setEmail("test-email");
        update.setAddress("updated-address");
        update.setPassword("updated-password");

        given(userRepository.findByEmail(anyString())).willReturn(Optional.of(user));
        given(userRepository.save(any(User.class))).willAnswer(invocationOnMock -> invocationOnMock.getArgument(0));
        given(passwordEncoder.encode(anyString())).willAnswer(invocationOnMock -> invocationOnMock.getArgument(0));

        //WHEN
        service.updatePassword(user, update);

        //THEN
        verify(userRepository, times(1)).findByEmail(anyString());
        verify(userRepository, times(1)).save(any(User.class));
        assertThat(user.getPassword()).isEqualTo("updated-password");
    }

    @Test
    void deleteAccount_should_throw_IllegalArgumentException_if_user_is_not_found() {
        //GIVEN
        User user = new User();
        user.setEmail("test-email");

        given(userRepository.findByEmail(anyString())).willReturn(Optional.empty());

        assertThatThrownBy(
                //WHEN
                () -> service.deleteAccount(user, "password"))
                //THEN
                .isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    void deleteAccount_should_throw_IllegalArgumentException_if_password_is_not_correct() {
        //GIVEN
        User user = new User();
        user.setEmail("test-email");
        user.setPassword("test-password");

        given(userRepository.findByEmail(anyString())).willReturn(Optional.of(user));
        given(passwordEncoder.matches("password", "test-password")).willReturn(false);

        assertThatThrownBy(
                //WHEN
                () -> service.deleteAccount(user, "password"))
                //THEN
                .isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    void deleteAccount_should_delegate_to_repository_and_delete_user() {
        //GIVEN
        User user = new User();
        user.setEmail("test-email");
        user.setPassword("test-password");
        user.setType(UserType.RESCUER);

        Animal dog = new Animal();
        Animal cat = new Animal();
        List<Animal> animals = new ArrayList<>();
        animals.add(dog);
        animals.add(cat);

        user.setAnimals(animals);

        given(userRepository.findByEmail(anyString())).willReturn(Optional.of(user));
        given(passwordEncoder.matches("test-password", "test-password")).willReturn(true);
        given(animalRepository.save(any(Animal.class))).willAnswer(invocationOnMock -> invocationOnMock.getArgument(0));

        //WHEN
        service.deleteAccount(user, "test-password");

        //THEN
        verify(userRepository, times(1)).findByEmail(anyString());
        verify(passwordEncoder, times(1)).matches(anyString(), anyString());
        verify(animalRepository, times(2)).save(any(Animal.class));
        verify(userRepository, times(1)).delete(user);
    }

    @Test
    void adminDeleteAllUsers_should_delegate_to_repository_and_delete_all_users() {
        //GIVEN
        User user = new User();
        user.setEmail("test-email");
        user.setType(UserType.RESCUER);

        Animal dog = new Animal();
        Animal cat = new Animal();
        List<Animal> animals = new ArrayList<>();
        animals.add(dog);
        animals.add(cat);

        user.setAnimals(animals);

        User user2 = new User();
        user2.setEmail("test-email-2");
        user2.setType(UserType.FINDER);

        Animal frog = new Animal();
        Animal bird = new Animal();
        List<Animal> animals2 = new ArrayList<>();
        animals.add(frog);
        animals.add(bird);

        user2.setAnimals(animals2);

        List<User> users = new ArrayList<>();
        users.add(user);
        users.add(user2);

        given(userRepository.findAll()).willReturn(users);
        given(animalRepository.save(any(Animal.class))).willAnswer(invocationOnMock -> invocationOnMock.getArgument(0));
        doNothing().when(userRepository).delete(any(User.class));

        //WHEN
        service.adminDeleteAllUsers();

        //THEN
        verify(userRepository, times(1)).findAll();
        verify(animalRepository, times(4)).save(any(Animal.class));
        verify(userRepository, times(2)).delete(any(User.class));
    }

    @Test
    void adminDeleteAccount_should_throw_IllegalArgumentException_if_user_is_not_found() {
        //GIVEN
        User user = new User();
        user.setEmail("test-email");

        given(userRepository.findByEmail(anyString())).willReturn(Optional.empty());

        assertThatThrownBy(
                //WHEN
                () -> service.adminDeleteAccount(user))
                //THEN
                .isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    void adminDeleteAccount_should_delegate_to_repository_and_delete_user_if_user_is_found() {
        //GIVEN
        User user = new User();
        user.setEmail("test-email");
        user.setType(UserType.FINDER);

        Animal dog = new Animal();
        Animal cat = new Animal();
        List<Animal> animals = new ArrayList<>();
        animals.add(dog);
        animals.add(cat);

        user.setAnimals(animals);

        given(userRepository.findByEmail(anyString())).willReturn(Optional.of(user));
        given(animalRepository.save(any(Animal.class))).willAnswer(invocationOnMock -> invocationOnMock.getArgument(0));
        doNothing().when(userRepository).delete(any(User.class));

        //WHEN
        service.adminDeleteAccount(user);

        //THEN
        verify(userRepository, times(1)).findByEmail(anyString());
        verify(animalRepository, times(2)).save(any(Animal.class));
        verify(userRepository, times(1)).delete(any(User.class));
    }

    @Test
    void showUsers_should_delegate_to_repository_and_show_users_by_given_type() {
        //GIVEN
        User user = new User();
        user.setEmail("test-email");
        user.setType(UserType.RESCUER);


        given(userRepository.findByType(UserType.RESCUER)).willReturn(Collections.singletonList(user));

        //WHEN
        List<User> foundUsers = service.showUsers(UserType.RESCUER);

        //THEN
        verify(userRepository, times(1)).findByType(UserType.RESCUER);
        assertThat(foundUsers.size()).isEqualTo(1);
    }

    @Test
    void rating_should_throw_IllegalArgumentException_if_user_is_not_found() {
        //GIVEN
        User user = new User();
        user.setEmail("test-email");

        given(userRepository.findByEmail(anyString())).willReturn(Optional.empty());

        assertThatThrownBy(
                //WHEN
                () -> service.rating(user))
                //THEN
                .isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    void rating_should_return_zero_if_userType_is_not_RESCUER() {
        //GIVEN
        User user = new User();
        user.setEmail("test-email");
        user.setType(UserType.FINDER);

        given(userRepository.findByEmail(anyString())).willReturn(Optional.of(user));

        //WHEN
        double rating = service.rating(user);

        //THEN
        verify(userRepository, times(1)).findByEmail(anyString());
        assertThat(rating).isEqualTo(0);
    }

    @Test
    void rating_should_return_percentage_if_userType_is_RESCUER() {
        //GIVEN
        User user = new User();
        user.setEmail("test-email");
        user.setType(UserType.RESCUER);
        user.setDeadCount(1);
        user.setCaredCount(2);

        given(userRepository.findByEmail(anyString())).willReturn(Optional.of(user));

        //WHEN
        double rating = service.rating(user);

        //THEN
        verify(userRepository, times(1)).findByEmail(anyString());
        assertThat(rating).isEqualTo(50);
    }

    @Test
    void findAll_should_delegate_to_repository_and_return_all_users() {
        //GIVEN
        User user = new User();
        user.setEmail("test-email");
        user.setType(UserType.FINDER);

        User user2 = new User();
        user2.setEmail("test-email");
        user2.setType(UserType.RESCUER);

        List<User> users = new ArrayList<>();
        users.add(user);
        users.add(user2);

        given(userRepository.findAll()).willReturn(users);

        //WHEN
        List<User> foundUsers = service.findAll();

        //THEN
        verify(userRepository, times(1)).findAll();
        assertThat(foundUsers.size()).isEqualTo(2);
    }

    @Test
    void allFilters_should_delegate_to_repository_and_show_filtered_users() {
        //GIVEN
        User user = new User();
        user.setEmail("test-email");
        user.setType(UserType.FINDER);
        user.setName("test-name-1");
        user.setProvince("test-province-1");

        User user2 = new User();
        user2.setEmail("test-email");
        user2.setType(UserType.RESCUER);
        user2.setName("test-name-2");
        user2.setProvince("test-province-2");

        List<User> users = new ArrayList<>();
        users.add(user);
        users.add(user2);

        given(userRepository.findAll()).willReturn(users);

        //WHEN
        List<User> foundUsers = service.allFilters("test-name-2", null);

        //THEN
        verify(userRepository, times(1)).findAll();
        assertThat(foundUsers.size()).isEqualTo(1);
    }

    @Test
    void resetAllUsersStatistics_should_delegate_to_repository_and_set_everything_to_zero() {
        //GIVEN
        User user = new User();
        user.setCaredCount(1);
        user.setReportedCount(2);
        user.setDeadCount(3);
        user.setReleasedCount(4);

        given(userRepository.findAll()).willReturn(Collections.singletonList(user));
        given(userRepository.save(any(User.class))).willAnswer(invocationOnMock -> invocationOnMock.getArgument(0));

        //WHEN
        service.resetAllUsersStatistics();

        //THEN
        verify(userRepository, times(1)).findAll();
        verify(userRepository, times(1)).save(any(User.class));
        assertAll(
                () -> assertThat(user.getCaredCount()).isEqualTo(0),
                () -> assertThat(user.getReportedCount()).isEqualTo(0),
                () -> assertThat(user.getDeadCount()).isEqualTo(0),
                () -> assertThat(user.getReleasedCount()).isEqualTo(0)
        );
    }

}
